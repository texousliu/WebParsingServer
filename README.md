### 说明 ###
- ~~因为项目用了 注册中心和配置中心，所以启动服务之前需要启动 注册中心和配置中心，具体的 demo 请看：
[open_demo](https://gitee.com/texousliu/open-demo.git) 
中的 demo-config-service 和 demo-eureka-service。~~
- ~~配置文件放在了 config 目录下~~
- 添加 gitlab ci/cd 方案，结合 helm、docker/k8s 实现持续集成
- 由于 k8s 中自有的负载以及 configMap 能够实现注册中心和配置中心的要求，因此取消了注册中心和配置中心
- 前端项目请查看 [book-store-front][book-store-front-link]
### 更新记录 ###
- 2020-04-26
    1. 修改书籍解析字段名称（以 bqb.json 为准）
    
- 2020-03-16
    1. 添加章节信息缓存
    1. 添加章节排序字段(number)生成
    1. 新增一些规划: 见 TODO

- 2020-03-14
    1. 服务化搜索解析逻辑,
    1. 新增 JsoupXpath 数字解析函数
    
- 2020-03-11
    1. 更新 mariadb_tables_create.sql 初始化语句
    1. 修改订阅发布
    1. 迁移 mongodb 存储到 mariadb（待测试）
    1. 新增 书架管理 功能接口（待测试）

- 2020-03-10
    1. chapter 表新增 url 链接
    1. 删除 origin 表
    1. 添加自有书源导入

- 2020-03-08
    1. 完成书源入库（由文件存储方式改为数据库存储）

- 2019-10-10
    1. 使用 checkstyle 规范化代码，checkstyle 配置详情请点击 [这里][checkstyle]

- 20190727 及以前

### 功能列表/计划安排 ###
1. （**已完成**） 书籍检索 
1. （**已完成**） 书籍目录检索 
1. （**已完成**） 章节内容检索
1. （**已完成**） 书籍入库操作
1. （**已完成**） 目录入库操作
1. （**已完成**） 章节入库操作
1. （**已完成：可以优化**） 解析器树划分
1. （**已完成**） 书源由文件迁移到数据库: 为实现自定义书源

1. 书源管理接口
    1. （**已完成**） 书源导入
    1. 书源编辑
    1. 删除书源
    
1. 用户阅读功能： 
    1. 用户管理，
        1. （**已完成**）用户注册
        1. （**已完成**）用户登录
    1. 用户书源自定义，
    1. （**已完成**） 用户书架，
    1. 用户个性化
    
1. 书海结构扩充： 分类，排行榜等功能
1. 存储方式由 mongodb 向 ES 转变：需要梳理存储结构
1. （**已完成**）添加 redis 实现用户上下文，缩短延时，简化接口
1. 添加 jsonpath 的支持，
1. 添加 xpath 转 jsonpath 操作，降低用户学习成本，统一解析风格
1. 图片另存，在考虑是否需要转存到 S3 或者是别的对象存储服务
1. （**已完成**） 拆分搜索逻辑，做到没有连数据库，检索功能也能用。
1. 添加内容矫正器
1. 添加章节序号解析器
1. 前端开发：打算使用 VUE
1. 添加缓存功能，减少请求解析时间
1. 添加下载功能
1. 添加搜索建议
1. 添加热搜排行
1. 添加换源功能


### 技术说明 ###
#### 设计模式使用：####
```
1、单例模式
2、原型模式
3、工厂方法
4、抽象工厂
5、建造者
6、模板方法
7、策略模式
8、观察者模式（发布订阅替代）
```

#### JsoupXpath 使用说明 ####
[JsoupXpath 官方开源库/使用说明][jsoup-xpath-link]

由于解析需要,所以项目实现了一些自定义 Function:
- tonumber("string", "type"): 中文数字转阿拉伯数字
    - type 表示类型; type = "1", 中文类型 = 一千万零二百, 转化后 10000200
    - type 表示类型; type = "2", 中文类型 = 一〇〇〇九, 转化后 10009

## 项目依赖说明
- [spring-boot-starter-extend starter扩展仓库][spring-boot-starter-extend]
- [commons 工具类仓库][commons]
- [archetype-stable Maven 骨架项目仓库][archetype-stable]
- [easy-talk 聊天仓库][easytalk]
- [open-demo 各种小例子仓库][open-demo]
- [web-parsing 网络爬虫仓库][web-parsing]
- [blog 博客文章仓库][blog]


[spring-boot-starter-extend]: https://gitee.com/texousliu/spring-boot-starter-extend
[commons]: https://gitee.com/texousliu/commons
[archetype-stable]: https://gitee.com/texousliu/archetype-stable
[easytalk]: https://gitee.com/texousliu/EasyTalkServer
[open-demo]: https://gitee.com/texousliu/open-demo
[web-parsing]: https://gitee.com/texousliu/WebParsingServer
[blog]: https://gitee.com/texousliu/blog
[checkstyle]: https://gitee.com/texousliu/blog/blob/master/checkstyle/checkstyle-825.xml
[jsoup-xpath-link]: https://github.com/zhegexiaohuozi/JsoupXpath
[book-store-front-link]: https://gitee.com/texousliu/book-store-front.git
