package cn.texous.web.parsing.engine.controller;

import cn.texous.util.commons.constant.Result;
import cn.texous.web.parsing.engine.common.utils.ContentProcessorUtils;
import cn.texous.web.parsing.engine.common.utils.RequesterUtils;
import cn.texous.web.parsing.engine.sdk.constants.RespType;
import cn.texous.web.parsing.engine.sdk.param.ParsingInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 收索接口
 *
 * @author Showa.L
 * @since 2019/7/24 17:05
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class ParsingApiController {

    @PostMapping("/parsing")
    public Result<String> fiction(@RequestBody @Valid ParsingInfo param) {
        long start = System.currentTimeMillis();
        // 发起请求
        String content = RequesterUtils.execute(param.getRequestInfo());
        RespType respType = RespType.getByCode(param.getRespType());
        // 处理内容
        String result = ContentProcessorUtils.process(content, respType, param.getParsingFieldInfos());
        log.info("【书籍搜索】：总耗时={}", System.currentTimeMillis() - start);
        // 返回处理结果
        return Result.ok(result);
    }

}
