package cn.texous.web.parsing.engine.config;

import cn.texous.web.parsing.engine.concurrent.AnaLogRunnable;
import cn.texous.web.parsing.engine.concurrent.AnaThreadPoolExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/24 17:43
 */
@Component
public class AnaLogInterceptor implements HandlerInterceptor {

    @Autowired
    private AnaThreadPoolExecutor threadPoolExecutor;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {
        threadPoolExecutor.execute(new AnaLogRunnable("请求拦截", 1));
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                Object handler, Exception ex) throws Exception {

    }
}
