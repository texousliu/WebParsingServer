package cn.texous.web.parsing.engine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * application main
 *
 * @author leven
 * @since
 */
@SpringBootApplication
public class WebParsingEngineApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebParsingEngineApplication.class, args);
    }

}
