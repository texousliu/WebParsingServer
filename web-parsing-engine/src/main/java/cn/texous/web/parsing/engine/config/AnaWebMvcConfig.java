package cn.texous.web.parsing.engine.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/24 17:49
 */
@Configuration
public class AnaWebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private AnaLogInterceptor logInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(logInterceptor).addPathPatterns("/api/**");
    }
}
