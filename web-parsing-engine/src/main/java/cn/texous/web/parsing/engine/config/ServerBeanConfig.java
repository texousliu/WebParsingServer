package cn.texous.web.parsing.engine.config;

import cn.texous.web.parsing.engine.xpath.ToNumber;
import org.hibernate.validator.HibernateValidator;
import org.seimicrawler.xpath.util.Scanner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/9/1 12:37
 */
@Configuration
public class ServerBeanConfig {

    /**
     * 配置 controller 参数校验
     *
     * @return
     */
    @Bean
    public Validator validator() {
        ValidatorFactory factory = Validation.byProvider(HibernateValidator.class)
                .configure()
                // 将fail_fast设置为true即可，如果想验证全部，则设置为false或者取消配置即可
                .addProperty("hibernate.validator.fail_fast", "true")
                .buildValidatorFactory();
        return factory.getValidator();
    }

    @Bean
    public Object addJsoupXpathFunction() {
        Scanner.registerFunction(ToNumber.class);

        return new Object();
    }

}
