package cn.texous.web.parsing.engine.concurrent;

import lombok.extern.slf4j.Slf4j;

/**
 * 日志存储线程
 *
 * @author leven
 * @since
 */
@Slf4j
public class AnaLogRunnable implements Runnable {

    private String content;
    private Integer type;

    public AnaLogRunnable(String content, Integer type) {
        this.content = content;
        this.type = type;
    }

    @Override
    public void run() {
        // TODO 使用 mongo 存储
        log.info("{} 日志类型：{}，内容：{}", Thread.currentThread().getName(), type, content);
    }
}
