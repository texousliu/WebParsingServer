package cn.texous.web.parsing.engine.config;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/29 11:32
 */
@Accessors(chain = true)
@Data
@Component
@ConfigurationProperties(prefix = "parsing.engine.config")
public class ServerConfig {

    private String serverName;
    private String serverVersion;

}
