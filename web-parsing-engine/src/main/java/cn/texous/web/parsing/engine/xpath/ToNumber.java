package cn.texous.web.parsing.engine.xpath;

import cn.texous.util.commons.util.ChineseUtils;
import lombok.extern.slf4j.Slf4j;
import org.seimicrawler.xpath.core.Function;
import org.seimicrawler.xpath.core.Scope;
import org.seimicrawler.xpath.core.XValue;

import java.util.List;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/12 17:16
 */
@Slf4j
public class ToNumber implements Function {

    @Override
    public String name() {
        return "tonumber";
    }

    @Override
    public XValue call(Scope scope, List<XValue> list) {
        String value = list.get(0).asString();
        try {
            if (list.size() > 1 && null != list.get(1)) {
                ChineseUtils.NumberType numberType = ChineseUtils.NumberType.loadByType(list.get(1).asString());
                return XValue.create(ChineseUtils.parseChineseToNumber(value, numberType));
            } else {
                return XValue.create(ChineseUtils.parseChineseToNumber(value, ChineseUtils.NumberType.NORMAL));
            }
        } catch (Exception var7) {
            log.error("parse chinese number to number error: {}", var7.getMessage());
        }
        return XValue.create(0);
    }

}
