package cn.texous.web.parsing.engine.concurrent;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/24 17:26
 */
@Slf4j
@Component
public class AnaThreadPoolExecutor {


    private static final Integer THREAD_POOL_SIZE = 10;
    private ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

    public void execute(Runnable runnable) {
        executorService.execute(runnable);
    }

}
