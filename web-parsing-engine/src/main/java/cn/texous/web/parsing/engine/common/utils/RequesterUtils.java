package cn.texous.web.parsing.engine.common.utils;

import cn.texous.util.commons.util.GsonUtils;
import cn.texous.util.commons.util.MessageUtil;
import cn.texous.web.parsing.engine.sdk.param.RequestInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 10:56
 */
@Slf4j
@Component
public class RequesterUtils {

    public static String execute(RequestInfo info) {
        long start = System.currentTimeMillis();
        log.info("发起请求：{}, 时间：{}", GsonUtils.toJson(info), start);
        MessageUtil.HttpRequestParam param = MessageUtil.HttpRequestParam.builder()
                .header(info.getHeaders())
                .outputStr(info.getBody())
                .requestMethod(info.getMethod())
                .requestUrl(info.getUrl())
                .inCharset(info.getInCharset() == null ? "UTF-8" : info.getInCharset())
                .outCharset(info.getOutCharset() == null ? "UTF-8" : info.getOutCharset())
                .build();
        String result = MessageUtil.commonRequest(param);
        log.info("结束请求所化时间：{}", System.currentTimeMillis() - start);
        return result;
    }

}
