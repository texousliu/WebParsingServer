package cn.texous.web.parsing.engine.jxpath;

import cn.texous.web.parsing.engine.xpath.ToNumber;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.seimicrawler.xpath.JXDocument;
import org.seimicrawler.xpath.JXNode;
import org.seimicrawler.xpath.exception.XpathSyntaxErrorException;
import org.seimicrawler.xpath.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * JXDocument Tester.
 *
 * @author github.com/zhegexiaohuozi seimimaster@gmail.com
 * @version 1.0
 */
public class JXDocumentTest {

    private JXDocument underTest;

    private JXDocument doubanTest;

    private JXDocument custom;
    private ClassLoader loader = getClass().getClassLoader();
    private Logger logger = LoggerFactory.getLogger(JXDocumentTest.class);

    @Before
    public void before() throws Exception {
        String html = "<html><body><div class='test'>一千二百三十二</div><div class='xiao'>Two</div></body></html>";
        underTest = JXDocument.create(html);
//        if (doubanTest == null) {
//            URL t = loader.getResource("d_test.html");
//            assert t != null;
//            File dBook = new File(t.toURI());
//            String context = FileUtils.readFileToString(dBook, Charset.forName("utf8"));
//            doubanTest = JXDocument.create(context);
//        }
        custom = JXDocument.create("<li><b>性别：</b>男</li>");
    }

    @Test
    public void testCustomerFunction() {
        Scanner.registerFunction(ToNumber.class);
        String xpath = "tonumber(//div[@class=\"test\"]/text())";
        List<JXNode> jxNodes = underTest.selN(xpath);
        jxNodes.forEach(jxNode -> System.out.println(jxNode.asDouble()));
    }


    /**
     * Method: sel(String xpath)
     */
    @Test
    public void testSel() throws Exception {
        String xpath = "//script[1]/text()";
        List<Object> res = underTest.sel(xpath);
        Assert.assertNotNull(res);
        Assert.assertTrue(res.size() > 0);
        logger.info(StringUtils.join(res, ","));
    }

    @Test
    public void testNotMatchFilter() throws Exception {
        String xpath = "//div[@class!~'xiao']/text()";
        List<Object> res = underTest.sel(xpath);
        Assert.assertEquals(1, res.size());
        logger.info(StringUtils.join(res, ","));
    }

    @Test
    public void testAs() throws XpathSyntaxErrorException {
        List<JXNode> jxNodeList = custom.selN("//b[contains(text(),'性别')]/parent::*/text()");
        Assert.assertEquals("男", StringUtils.join(jxNodeList, ""));
        for (JXNode jxNode : jxNodeList) {
            logger.info(jxNode.toString());
        }
    }

    /**
     * fix https://github.com/zhegexiaohuozi/JsoupXpath/issues/33
     */
//    @Test
    public void testNotObj() {
        JXDocument doc = JXDocument.createByUrl("https://www.gxwztv.com/61/61514/");
//        List<JXNode> nodes = doc.selN("//*[@id=\"chapters-list\"]/li[@style]");
        List<JXNode> nodes = doc.selN("//*[@id=\"chapters-list\"]/li[not(@style)]");
        for (JXNode node : nodes) {
            logger.info("r = {}", node);
        }
    }

    /**
     * fix https://github.com/zhegexiaohuozi/JsoupXpath/issues/34
     */
    @Test
    public void testAttrAtRoot() {
        String content = "<html>\n" +
                " <head></head>\n" +
                " <body>\n" +
                "  <a href=\"/124/124818/162585930.html\">第2章 神奇交流群</a>\n" +
                " </body>\n" +
                "</html>";
        JXDocument doc = JXDocument.create(content);
        List<JXNode> nodes = doc.selN("//@href");
        for (JXNode node : nodes) {
            logger.info("r = {}", node);
        }
    }

    @Test
    public void testA() {
        String content = "<span style=\"color: #5191ce;\" >网页设计师</span>";
        JXDocument doc = JXDocument.create(content);
        List<JXNode> nodes = doc.selN("//*[text()='网页设计师']");
        for (JXNode node : nodes) {
            logger.info("r = {}", node);
        }
    }

}
