package cn.texous.web.parsing.engine.sdk.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/3 17:59
 */
@Getter
@AllArgsConstructor
public enum RespType {

    JSON("json"),
    HTML("html"),
    ;

    private String code;

    public static RespType getByCode(String code) {
        return Stream.of(RespType.values()).filter(rt -> rt.getCode().equals(code))
                .findAny()
                .orElse(RespType.HTML);
    }

}
