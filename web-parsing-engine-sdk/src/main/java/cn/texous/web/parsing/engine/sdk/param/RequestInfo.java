package cn.texous.web.parsing.engine.sdk.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Map;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 10:25
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestInfo implements Serializable {

    private static final long serialVersionUID = 1333171135249635929L;

    /**
     * 请求方式 GET/POST
     */
    @NotNull(message = "requestInfo method is required")
    private String method;
    /**
     * 请求链接
     */
    @NotNull(message = "requestInfo url is required")
    private String url;
    /**
     * 请求头
     */
    private Map<String, String> headers;
    /**
     * 请求体
     */
    private String body;
    /**
     * 请求编码
     */
    private String inCharset;
    /**
     * 请求编码
     */
    private String outCharset;

}
