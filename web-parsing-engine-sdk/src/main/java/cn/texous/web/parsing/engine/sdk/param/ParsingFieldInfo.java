package cn.texous.web.parsing.engine.sdk.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 请求结果解析器
 *
 * @author Showa.L
 * @since 2019/7/26 14:32
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParsingFieldInfo implements Serializable {

    private static final long serialVersionUID = -618308571047173657L;

    /**
     * 是否需要转节点
     */
    @NotNull(message = "parsingFieldInfo changeNode is required")
    private boolean changeNode;
    /**
     * xpath
     */
    private String xpath;
    /**
     * 节点对应的属性
     */
    @NotNull(message = "parsingFieldInfo field is required")
    private String field;
    /**
     * 子解析器
     */
    private List<ParsingFieldInfo> childs;

}
