package ${basePackage}.service.impl.mysql;

import ${basePackage}.dao.mysql.${modelNameUpperCamel}Mapper;
import ${basePackage}.model.entity.mysql.${modelNameUpperCamel};
import ${basePackage}.service.api.mysql.${modelNameUpperCamel}Service;
import ${basePackage}.config.mysql.AbstractService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * insert description here.
 *
 * @author ${author}
 * @since ${date}
 */
@Service
public class ${modelNameUpperCamel}ServiceImpl
        extends AbstractService<${modelNameUpperCamel}> implements ${modelNameUpperCamel}Service {

    @Resource
    private ${modelNameUpperCamel}Mapper ${modelNameLowerCamel}Mapper;

}
