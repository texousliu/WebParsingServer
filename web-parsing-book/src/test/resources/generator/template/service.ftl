package ${basePackage}.service.api.mysql;

import ${basePackage}.model.entity.mysql.${modelNameUpperCamel};
import ${basePackage}.config.mysql.Service;


/**
 * insert description here.
 *
 * @author ${author}
 * @since ${date}
 */
public interface ${modelNameUpperCamel}Service extends Service<${modelNameUpperCamel}> {

}
