package cn.texous.web.parsing.book.controller;

import cn.texous.web.parsing.book.model.param.AnaSearchFictionParam;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/25 14:23
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AnaSearchControllerTest {

    @Autowired
    private WebApplicationContext wac;

    protected MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }


    @Test
    public void fiction() throws Exception {
        AnaSearchFictionParam param = new AnaSearchFictionParam();
        param.setCondition("王道");
        param.setOrigin("ucsm");
        param.setType(1);
        MvcResult result = mockMvc
                .perform(MockMvcRequestBuilders.post("/search/fiction")
                        .content(new ObjectMapper().writeValueAsString(param))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
//            .andDo(p -> print(p.getResponse().getContentAsString()))
                .andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        System.out.println(contentAsString);
    }

    @Test
    public void catalog() {
    }

    @Test
    public void chapter() {
    }
}
