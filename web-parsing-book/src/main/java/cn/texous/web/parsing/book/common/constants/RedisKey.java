package cn.texous.web.parsing.book.common.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/12 11:31
 */
@Getter
@AllArgsConstructor
public enum RedisKey {

    BOOK_CODE_KEY_PREFIX("ANA_SYSTEM:BOOK_CODE_KEY_PREFIX:", "存储书籍CODE，根据 origin name author Md5 加密"),
    BOOK_INFO_KEY_PREFIX("ANA_SYSTEM:BOOK_INFO_KEY_PREFIX:", "存储书籍信息，根据 CODE "),
    CHAPTERS_INFO_KEY_PREFIX("ANA_SYSTEM:CHAPTERS_INFO_KEY_PREFIX:", "存储书籍章节信息"),

    LOGIN_USER_INFO_KEY_PREFIX("ANA_SYSTEM:LOGIN_USER_INFO_KEY_PREFIX:", "存储书籍章节信息"),
    ;

    private String prefix;
    private String desc;

}
