package cn.texous.web.parsing.book.model.json.self;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 11:16
 */
@Data
public class SelfChapterInfo implements Serializable {

    private static final long serialVersionUID = 8056336662278420422L;
    private SelfHttpInfo httpInfo;
    private SelfHttpInfo nextPageInfo;
    private List<SelfFieldInfo> fieldInfos;
    private Integer weights;
    private Integer retry;
    private String dataPattern;

}
