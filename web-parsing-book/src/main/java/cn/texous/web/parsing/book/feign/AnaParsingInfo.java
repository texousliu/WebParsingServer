package cn.texous.web.parsing.book.feign;

import cn.texous.web.parsing.book.model.parser.AnaRequestInfo;
import cn.texous.web.parsing.book.model.parser.origin.AnaOriginParserFieldInfo;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * /api/parsing 接口请求参数
 *
 * @author Showa.L
 * @since 2020/3/13 11:23
 */
@Data
public class AnaParsingInfo implements Serializable {

    private static final long serialVersionUID = -5913652616050575210L;
    /**
     * 返回结果类型：json/html
     */
    @NotNull(message = "respType is required")
    private String respType;
    /**
     * 需要发起的请求信息
     */
    @NotNull(message = "requestInfo is required")
    @Valid
    private AnaRequestInfo requestInfo;
    /**
     * 请求结果解析器列表
     */
    @NotNull(message = "parsingFieldInfos is required")
    @Valid
    private List<AnaOriginParserFieldInfo> parsingFieldInfos;

}
