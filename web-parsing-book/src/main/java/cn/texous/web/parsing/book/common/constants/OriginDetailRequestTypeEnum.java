package cn.texous.web.parsing.book.common.constants;

import cn.texous.util.commons.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/3 18:03
 */
@Getter
@AllArgsConstructor
public enum OriginDetailRequestTypeEnum {

    //1=常规请求，2=下一页，3=上一页
    NORMAL(1, "常规请求"),
    NEXT(2, "下一页"),
    PRE(3, "上一页"),
    ;

    private int code;
    private String desc;

    public static OriginDetailRequestTypeEnum loadByCode(int code) {
        return Stream.of(OriginDetailRequestTypeEnum.values())
                .filter(r -> r.getCode() == code)
                .findAny()
                .orElseThrow(() -> new BusinessException(
                        AnaResultCode.FS_ORIGIN_DETAIL_REQUEST_TYPE_ERROR));
    }

}
