package cn.texous.web.parsing.book.common.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/11 14:22
 */
@Getter
@AllArgsConstructor
public enum AnaBookStatusEnum {

    PREPARE("PREPARE", "待获取"),
    LOADING("LOADING", "连载中"),
    FINISH("FINISH", "已完本"),
    ;

    private String code;
    private String desc;

}
