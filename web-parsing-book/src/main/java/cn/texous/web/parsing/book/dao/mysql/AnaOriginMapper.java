package cn.texous.web.parsing.book.dao.mysql;

import cn.texous.web.parsing.book.config.mysql.MyMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOrigin;
import cn.texous.web.parsing.book.model.vo.AnaOriginDictVO;

import java.util.List;
import java.util.Map;

public interface AnaOriginMapper extends MyMapper<AnaOrigin> {

    List<AnaOrigin> selectByParam(Map<String, Object> params);

    List<AnaOriginDictVO> selectOriginList();

}