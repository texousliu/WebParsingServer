package cn.texous.web.parsing.book.model.entity.mysql;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "ana_book")
public class AnaBook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 书源类型
     */
    @Column(name = "ana_origin_code")
    private String anaOriginCode;

    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 作者
     */
    private String author;

    /**
     * 封面
     */
    @Column(name = "title_page")
    private String titlePage;

    /**
     * 字数(个)
     */
    private Integer words;

    /**
     * 摘要（简介）
     */
    private String summary;

    /**
     * 状态：PREPARE=带获取，LOADING=连载中，FINISH=已完本
     */
    private String status;

    @Column(name = "catalog_url")
    private String catalogUrl;

    /**
     * 创建时间
     */
    private Date created;

    /**
     * 更新时间
     */
    private Date updated;
}
