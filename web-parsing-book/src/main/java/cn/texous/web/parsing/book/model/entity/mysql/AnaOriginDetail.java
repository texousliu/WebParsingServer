package cn.texous.web.parsing.book.model.entity.mysql;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "ana_origin_detail")
public class AnaOriginDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 数据源id
     */
    @Column(name = "ana_origin_id")
    private Integer anaOriginId;

    /**
     * 类型：1=书籍，2=目录，3=章节
     */
    private Integer type;

    /**
     * 权重：数值越大权重越高
     */
    private Integer weights;

    /**
     * 重试次数：0=不重试
     */
    private Integer retry;

    /**
     * 时间表达式
     */
    @Column(name = "date_pattern")
    private String datePattern;

    /**
     * 状态：0=不可用，1=可用，2=已删除
     */
    private Integer status;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;
}
