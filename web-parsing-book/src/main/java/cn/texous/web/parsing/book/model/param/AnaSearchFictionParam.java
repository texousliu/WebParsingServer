package cn.texous.web.parsing.book.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/24 17:13
 */
@Data
@ApiModel
public class AnaSearchFictionParam implements Param, Serializable {

    @ApiModelProperty(
            required = true,
            notes = "查询条件",
            example = "踏星"
    )
    @NotNull(message = "condition is required")
    private String condition;

    @ApiModelProperty(
            notes = "查询类型：1=名称，2=作者，3=猪脚. 具体见字典接口 searchType",
            example = "1"
    )
    private Integer type;

    @ApiModelProperty(
            required = true,
            notes = "搜索源: 具体见字典接口 origin",
            example = "bqb"
    )
    @NotNull(message = "origin is required")
    private String origin;

    @ApiModelProperty(
            notes = "页码: 不传表示第一页",
            example = "1"
    )
    private Integer page;

    public Integer getType() {
        return type == null ? 1 : type;
    }
}
