package cn.texous.web.parsing.book.model.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@ApiModel
public class AnaReadResp {

    @ApiModelProperty(
            notes = "id",
            example = "1"
    )
    private Integer id;

    @ApiModelProperty(
            notes = "书籍编码",
            example = "2020123123123123"
    )
    private String anaBookCode;

    @ApiModelProperty(
            notes = "章节编码",
            example = "2020723494456935"
    )
    private String code;

    @ApiModelProperty(
            notes = "章节排序",
            example = "1"
    )
    private Integer number;

    @ApiModelProperty(
            notes = "章节名称",
            example = "第一章 百里守约"
    )
    private String name;

    @ApiModelProperty(
            notes = "章节字数",
            example = "3159"
    )
    private Integer words;

    @ApiModelProperty(
            notes = "请求链接",
            example = "http://chapter.test.com/1.html"
    )
    private String url;

    @ApiModelProperty(
            notes = "章节内容",
            example = "这是一个夜黑风高的晚上..."
    )
    private String content;

    @ApiModelProperty(
            notes = "创建时间",
            example = "2020-03-16 11:11:11"
    )
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date created;

    @ApiModelProperty(
            notes = "更新时间",
            example = "2020-0316 12:11:11"
    )
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updated;
}
