package cn.texous.web.parsing.book.service.impl.mysql;

import cn.texous.web.parsing.book.config.mysql.AbstractService;
import cn.texous.web.parsing.book.dao.mysql.AnaBookMapper;
import cn.texous.web.parsing.book.manager.RedisManager;
import cn.texous.web.parsing.book.model.entity.mysql.AnaBook;
import cn.texous.web.parsing.book.service.api.mysql.AnaBookService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-11 14:20:37
 */
@Service
public class AnaBookServiceImpl
        extends AbstractService<AnaBook> implements AnaBookService {

    @Resource
    private AnaBookMapper anaBookMapper;

    @Override
    public AnaBook findByUnique(String origin, String name, String author) {
        return anaBookMapper.findByUnique(origin, name, author);
    }

    @Override
    public List<AnaBook> findLimit(Integer page, Integer size) {
        Integer offset = (page - 1) * size;
        return anaBookMapper.findLimit(offset, size);
    }

    @Override
    public AnaBook findByCache(String code) {
        AnaBook book = RedisManager.getBookInfo(code);
        if (book == null) {
            book = findByCodeAndCache(code);
        }
        return book;
    }

    private AnaBook findByCodeAndCache(String code) {
        AnaBook anaBook = new AnaBook();
        anaBook.setCode(code);
        AnaBook book = anaBookMapper.selectOne(anaBook);
        if (book != null)
            RedisManager.cacheBookInfo(book);
        return book;
    }
}
