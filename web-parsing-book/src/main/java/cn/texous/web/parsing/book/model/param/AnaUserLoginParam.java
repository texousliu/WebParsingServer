package cn.texous.web.parsing.book.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel
public class AnaUserLoginParam {

    @ApiModelProperty(
            required = true,
            notes = "密码",
            example = "123.com"
    )
    @NotBlank(message = "password is required")
    private String password;

    @ApiModelProperty(
            required = true,
            notes = "手机号",
            example = "18900000000"
    )
    @NotBlank(message = "phoneNumber is required")
    @Length(min = 11, max = 11, message = "length isn't accept")
//    @Min(value = 11, message = "length isn't accept")
//    @Max(value = 11, message = "length isn't accept")
    private String phoneNumber;

}
