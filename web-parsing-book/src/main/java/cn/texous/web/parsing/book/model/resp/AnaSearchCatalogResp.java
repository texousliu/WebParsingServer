package cn.texous.web.parsing.book.model.resp;

import cn.texous.web.parsing.book.model.dto.AnaSearchChapterDTO;
import lombok.Data;

import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 14:01
 */
@Data
public class AnaSearchCatalogResp implements Resp {

    private List<AnaSearchChapterDTO> chapters;

}
