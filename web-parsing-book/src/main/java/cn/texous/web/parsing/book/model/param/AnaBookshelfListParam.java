package cn.texous.web.parsing.book.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/11 17:12
 */
@Data
@ApiModel
public class AnaBookshelfListParam {

    @ApiModelProperty(
            notes = "书籍名称",
            example = "踏星"
    )
    private String anaBookName;

}
