package cn.texous.web.parsing.book.strategy.search;

import cn.texous.util.commons.constant.Result;
import cn.texous.web.parsing.book.model.parser.origin.AnaOriginParser;
import cn.texous.web.parsing.book.model.resp.Resp;

import java.util.Map;

/**
 * 内容处理器，提取请求结果
 *
 * @author Showa.L
 * @since 2020/3/6 17:35
 */
public interface ContentProcessor<R extends Resp> {

    R process(Result<String> content, AnaOriginParser originParser, Map<String, Object> otherInfo);

}
