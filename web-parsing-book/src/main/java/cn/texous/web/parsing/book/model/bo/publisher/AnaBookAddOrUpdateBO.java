package cn.texous.web.parsing.book.model.bo.publisher;

import cn.texous.web.parsing.book.model.dto.AnaSearchBookDTO;
import lombok.Data;

import java.util.List;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/7/27 17:34
 */
@Data
public class AnaBookAddOrUpdateBO implements EventBO {

    private List<AnaSearchBookDTO> books;
    private String origin;

}
