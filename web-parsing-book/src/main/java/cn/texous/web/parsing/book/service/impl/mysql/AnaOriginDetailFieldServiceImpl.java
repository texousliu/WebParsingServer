package cn.texous.web.parsing.book.service.impl.mysql;

import cn.texous.web.parsing.book.config.mysql.AbstractService;
import cn.texous.web.parsing.book.dao.mysql.AnaOriginDetailFieldMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOriginDetailField;
import cn.texous.web.parsing.book.service.api.mysql.AnaOriginDetailFieldService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-03 16:17:56
 */
@Service
public class AnaOriginDetailFieldServiceImpl
        extends AbstractService<AnaOriginDetailField> implements AnaOriginDetailFieldService {

    @Resource
    private AnaOriginDetailFieldMapper anaOriginDetailFieldMapper;

}
