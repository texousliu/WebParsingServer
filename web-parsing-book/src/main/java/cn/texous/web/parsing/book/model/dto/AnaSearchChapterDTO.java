package cn.texous.web.parsing.book.model.dto;

import lombok.Data;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 18:25
 */
@Data
public class AnaSearchChapterDTO {

    private String code;
    private String name;
    private String url;

}
