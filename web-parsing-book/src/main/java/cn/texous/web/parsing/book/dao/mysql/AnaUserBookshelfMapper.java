package cn.texous.web.parsing.book.dao.mysql;

import cn.texous.web.parsing.book.config.mysql.MyMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaUserBookshelf;
import cn.texous.web.parsing.book.model.vo.AnaUserBookVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface AnaUserBookshelfMapper extends MyMapper<AnaUserBookshelf> {

    AnaUserBookshelf findByUnique(@Param("anaUserId") Integer anaUserId,
                                  @Param("anaBookCode") String anaBookCode);

    List<AnaUserBookVO> findByParam(Map<String, Object> params);

}