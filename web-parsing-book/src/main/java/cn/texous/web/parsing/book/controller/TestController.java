package cn.texous.web.parsing.book.controller;

import cn.texous.util.commons.constant.Result;
import cn.texous.web.parsing.book.feign.AnaParsingInfo;
import cn.texous.web.parsing.book.feign.ParsingEngineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/13 18:05
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private ParsingEngineService parsingEngineService;

    @PostMapping("/test")
    public Result<String> parsing(@RequestBody @Valid AnaParsingInfo parsingInfo) {

        try {
            return parsingEngineService.parsing(parsingInfo).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Result.fail("dddddd");
    }

}
