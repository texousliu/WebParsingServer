package cn.texous.web.parsing.book.strategy.search.fiction;

import cn.texous.util.commons.exception.BusinessException;
import cn.texous.util.commons.util.ConverterUtils;
import cn.texous.web.parsing.book.common.constants.AnaResultCode;
import cn.texous.web.parsing.book.common.constants.OriginDetailRequestTypeEnum;
import cn.texous.web.parsing.book.common.constants.OriginDetailTypeEnum;
import cn.texous.web.parsing.book.feign.AnaParsingInfo;
import cn.texous.web.parsing.book.manager.RedisManager;
import cn.texous.web.parsing.book.model.entity.mysql.AnaBook;
import cn.texous.web.parsing.book.model.param.AnaSearchCatalogParam;
import cn.texous.web.parsing.book.model.parser.AnaRequestInfo;
import cn.texous.web.parsing.book.model.parser.origin.AnaOriginParser;
import cn.texous.web.parsing.book.model.parser.origin.AnaOriginParserModuleInfo;
import cn.texous.web.parsing.book.model.parser.origin.AnaOriginParserRequestInfo;
import cn.texous.web.parsing.book.service.api.mysql.AnaBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 10:56
 */
@Component(CatalogAnaRequester.CAR)
public class CatalogAnaRequester extends AbstractAnaRequester<AnaSearchCatalogParam> {

    /**
     * bean name
     */
    public static final String CAR = "catalogAnaRequester";

    @Autowired
    private AnaBookService anaBookService;

    @Override
    public AnaParsingInfo getRequestInfo(AnaSearchCatalogParam searchParam,
                                         AnaOriginParser originParser) {
        AnaOriginParserModuleInfo moduleInfo = originParser
                .getModuleInfoByType(OriginDetailTypeEnum.CATALOG);
        AnaOriginParserRequestInfo parserRequestInfo = moduleInfo
                .getRequestInfoByType(OriginDetailRequestTypeEnum.NORMAL);

        AnaRequestInfo requestInfo = ConverterUtils
                .convert(AnaRequestInfo.class, parserRequestInfo);
        AnaBook anaBook = anaBookService.findByCache(searchParam.getAnaBookCode());
        if (anaBook == null)
            throw new BusinessException(AnaResultCode.FS_FICTION_NOTFIND);
        requestInfo.setUrl(anaBook.getCatalogUrl());

        addHttpCharset(parserRequestInfo, requestInfo, originParser.getBaseInfo());

        AnaParsingInfo parsingInfo = new AnaParsingInfo();
        parsingInfo.setRespType(originParser.getBaseInfo().getRespType());
        parsingInfo.setRequestInfo(requestInfo);
        parsingInfo.setParsingFieldInfos(moduleInfo.getAnaFieldInfos());

        return parsingInfo;
    }

}
