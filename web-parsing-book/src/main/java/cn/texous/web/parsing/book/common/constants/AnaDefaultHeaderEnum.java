package cn.texous.web.parsing.book.common.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2020/4/1 10:24
 */
@Getter
@AllArgsConstructor
public enum AnaDefaultHeaderEnum {

    BIRD("https://leven-test-bucket.nos-eastchina1.126.net/reader/bird.png"),
    BLACK_CAT("https://leven-test-bucket.nos-eastchina1.126.net/reader/black-cat.png"),
    BULLDOG("https://leven-test-bucket.nos-eastchina1.126.net/reader/bulldog.png"),
    BUNNY("https://leven-test-bucket.nos-eastchina1.126.net/reader/bunny.png"),
    CARDINAL("https://leven-test-bucket.nos-eastchina1.126.net/reader/cardinal.png"),
    CAT("https://leven-test-bucket.nos-eastchina1.126.net/reader/cat.png"),
    CHICKEN("https://leven-test-bucket.nos-eastchina1.126.net/reader/chicken.png"),
    CHIHUAHUA("https://leven-test-bucket.nos-eastchina1.126.net/reader/chihuahua.png"),
    COW("https://leven-test-bucket.nos-eastchina1.126.net/reader/cow.png"),
    CRAB("https://leven-test-bucket.nos-eastchina1.126.net/reader/crab.png"),
    DACHSHUND("https://leven-test-bucket.nos-eastchina1.126.net/reader/dachshund.png"),
    DOG("https://leven-test-bucket.nos-eastchina1.126.net/reader/dog.png"),
    DOLPHIN("https://leven-test-bucket.nos-eastchina1.126.net/reader/dolphin.png"),
    DONKEY("https://leven-test-bucket.nos-eastchina1.126.net/reader/donkey.png"),
    DUCK("https://leven-test-bucket.nos-eastchina1.126.net/reader/duck.png"),
    FISH("https://leven-test-bucket.nos-eastchina1.126.net/reader/fish.png"),
    FROG("https://leven-test-bucket.nos-eastchina1.126.net/reader/frog.png"),
    GOLD_FISH("https://leven-test-bucket.nos-eastchina1.126.net/reader/gold-fish.png"),
    HAMSTER("https://leven-test-bucket.nos-eastchina1.126.net/reader/hamster.png"),
    HORSE("https://leven-test-bucket.nos-eastchina1.126.net/reader/horse.png"),
    JELLYFISH("https://leven-test-bucket.nos-eastchina1.126.net/reader/jellyfish.png"),
    KITTEN("https://leven-test-bucket.nos-eastchina1.126.net/reader/kitten.png"),
    LOBSTER("https://leven-test-bucket.nos-eastchina1.126.net/reader/lobster.png"),
    MOUSE("https://leven-test-bucket.nos-eastchina1.126.net/reader/mouse.png"),
    OCTOPUS("https://leven-test-bucket.nos-eastchina1.126.net/reader/octopus.png"),
    PARROT("https://leven-test-bucket.nos-eastchina1.126.net/reader/parrot.png"),
    PIG("https://leven-test-bucket.nos-eastchina1.126.net/reader/pig.png"),
    PONY("https://leven-test-bucket.nos-eastchina1.126.net/reader/pony.png"),
    PUPPY("https://leven-test-bucket.nos-eastchina1.126.net/reader/puppy.png"),
    RABBIT("https://leven-test-bucket.nos-eastchina1.126.net/reader/rabbit.png"),
    SEAL("https://leven-test-bucket.nos-eastchina1.126.net/reader/seal.png"),
    SHARK("https://leven-test-bucket.nos-eastchina1.126.net/reader/shark.png"),
    SHEEP("https://leven-test-bucket.nos-eastchina1.126.net/reader/sheep.png"),
    SQUID("https://leven-test-bucket.nos-eastchina1.126.net/reader/squid.png"),
    SQUIRREL("https://leven-test-bucket.nos-eastchina1.126.net/reader/squirrel.png"),
    TROPICAL_FISH("https://leven-test-bucket.nos-eastchina1.126.net/reader/tropical-fish.png"),
    TUNA("https://leven-test-bucket.nos-eastchina1.126.net/reader/tuna.png"),
    TURKEY("https://leven-test-bucket.nos-eastchina1.126.net/reader/turkey.png"),
    TURTLE("https://leven-test-bucket.nos-eastchina1.126.net/reader/turtle.png"),
    WHALE("https://leven-test-bucket.nos-eastchina1.126.net/reader/whale.png"),
    ;

    private String url;

}
