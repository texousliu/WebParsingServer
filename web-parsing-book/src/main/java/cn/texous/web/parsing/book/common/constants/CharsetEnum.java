package cn.texous.web.parsing.book.common.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/3 17:55
 */
@Getter
@AllArgsConstructor
public enum CharsetEnum {

    UTF_8("UTF-8"),
    GBK("GBK"),
    IOS_8859_1("ISO-8859-1"),
    US_ASCII("US-ASCII"),
    UTF16("UTF-16"),
    UTF16BE("UTF-16BE"),
    UTF16LE("UTF-16LE"),
    ;

    private String code;


}
