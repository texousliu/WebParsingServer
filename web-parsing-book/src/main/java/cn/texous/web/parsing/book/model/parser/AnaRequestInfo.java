package cn.texous.web.parsing.book.model.parser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 10:25
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnaRequestInfo implements Serializable {

    private static final long serialVersionUID = -1564517485580644956L;
    private String method;
    private String url;
    private Map<String, String> headers;
    private String body;
    private String inCharset;
    private String outCharset;

}
