package cn.texous.web.parsing.book.service.api.mysql;

import cn.texous.web.parsing.book.config.mysql.Service;
import cn.texous.web.parsing.book.model.entity.mysql.AnaBook;

import java.util.List;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-11 14:20:37
 */
public interface AnaBookService extends Service<AnaBook> {

    /**
     * 根据唯一索引查找书籍
     *
     * @param origin 数据源
     * @param name   书籍名称
     * @param author 书籍作者
     * @return
     */
    AnaBook findByUnique(String origin, String name, String author);

    List<AnaBook> findLimit(Integer page, Integer size);

    AnaBook findByCache(String code);

}
