package cn.texous.web.parsing.book.dao.mysql;

import cn.texous.web.parsing.book.config.mysql.MyMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaUserBook;
import org.apache.ibatis.annotations.Param;

public interface AnaUserBookMapper extends MyMapper<AnaUserBook> {

    int insertOrUpdate(AnaUserBook anaUserBook);

    AnaUserBook getReadHistory(@Param("anaUserId") Integer anaUserId,
                               @Param("anaBookCode") String anaBookCode);

}