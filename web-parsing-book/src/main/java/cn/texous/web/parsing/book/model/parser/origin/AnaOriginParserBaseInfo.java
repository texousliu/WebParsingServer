package cn.texous.web.parsing.book.model.parser.origin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 11:13
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnaOriginParserBaseInfo implements Serializable {

    private static final long serialVersionUID = 1786047890942647270L;
    private String name; // ": "uc",
    private String domain; // ": "www.uctxt.com",
    private Boolean openSSL; // ": false,
    private Integer weights; // ": 1,
    private Integer retry; // ": 0,
    private String datePattern; // ": "yyyy-MM-dd",
    private String description; // ": "说明",
    private String charset; // ": "UTF-8",
    private String respType; // ": "json/xml/html",
    private Boolean dynamicAnalysis; // ": true,
    private Boolean openCookies; // ": false,
    private String urlEncoder; //": "utf-8",
    private Map<String, String> customizeHeader; // ": null

}
