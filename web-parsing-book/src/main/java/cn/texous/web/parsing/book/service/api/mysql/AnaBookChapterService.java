package cn.texous.web.parsing.book.service.api.mysql;

import cn.texous.web.parsing.book.config.mysql.Service;
import cn.texous.web.parsing.book.model.entity.mysql.AnaBookChapter;
import cn.texous.web.parsing.book.model.resp.AnaSearchCatalogResp;
import cn.texous.web.parsing.book.model.resp.AnaSearchChapterResp;

import java.util.List;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-11 14:20:37
 */
public interface AnaBookChapterService extends Service<AnaBookChapter> {

    /**
     * 根据唯一索引查找章节
     *
     * @param bookCode    书籍编码
     * @param chapterName 章节标题
     * @return
     */
    AnaBookChapter findByUnique(String bookCode, String chapterName);

    /**
     * 根据 书籍 code 查找章节列表
     *
     * @param bookCode 书籍code
     * @return
     */
    List<AnaBookChapter> findByBookCodeCache(String bookCode);

    /**
     * 获取章节目录，先走缓存
     *
     * @param bookCode 书本 code
     * @return
     */
    AnaSearchCatalogResp findCatalogRespByCache(String bookCode);

    /**
     * 获取章节信息
     *
     * @param code 章节 code
     * @return
     */
    AnaSearchChapterResp findByCode(String code);

    /**
     * 根据 code 更新内容
     *
     * @param anaBookChapter 需要更新的内容
     * @return
     */
    Boolean updateByCode(AnaBookChapter anaBookChapter);

    /**
     * 批量插入
     *
     * @param anaBookChapters 章节信息表
     * @return
     */
    Integer insertIgnore(List<AnaBookChapter> anaBookChapters);

    /**
     * 获取书籍第一章
     *
     * @param bookCode 书籍code
     * @return
     */
    AnaBookChapter getFirstChapterByCache(String bookCode);

}
