package cn.texous.web.parsing.book.dao.mysql;

import cn.texous.web.parsing.book.config.mysql.MyMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOriginField;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AnaOriginFieldMapper extends MyMapper<AnaOriginField> {

    List<AnaOriginField> findByType(@Param("type") Integer type);

}