package cn.texous.web.parsing.book.service.api.mysql;

import cn.texous.web.parsing.book.config.mysql.Service;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOriginDetail;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-03 16:17:56
 */
public interface AnaOriginDetailService extends Service<AnaOriginDetail> {

}
