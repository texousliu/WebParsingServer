package cn.texous.web.parsing.book.service.impl.mysql;

import cn.texous.web.parsing.book.config.mysql.AbstractService;
import cn.texous.web.parsing.book.dao.mysql.AnaOriginDetailMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOriginDetail;
import cn.texous.web.parsing.book.service.api.mysql.AnaOriginDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-03 16:17:56
 */
@Service
public class AnaOriginDetailServiceImpl
        extends AbstractService<AnaOriginDetail> implements AnaOriginDetailService {

    @Resource
    private AnaOriginDetailMapper anaOriginDetailMapper;

}
