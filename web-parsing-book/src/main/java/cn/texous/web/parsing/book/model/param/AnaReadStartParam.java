package cn.texous.web.parsing.book.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2020-03-16 11:32
 */
@Data
@ApiModel
public class AnaReadStartParam {

    @ApiModelProperty(
            required = true,
            notes = "书籍编码",
            example = "202012323123123123"
    )
    @NotNull(message = "bookCode is required")
    private String bookCode;

}
