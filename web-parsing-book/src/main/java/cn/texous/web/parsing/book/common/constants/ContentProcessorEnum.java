package cn.texous.web.parsing.book.common.constants;

import cn.texous.web.parsing.book.strategy.search.fiction.CatalogContentProcessor;
import cn.texous.web.parsing.book.strategy.search.fiction.ChapterContentProcessor;
import cn.texous.web.parsing.book.strategy.search.fiction.FictionContentProcessor;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/7/27 16:47
 */
@Getter
@AllArgsConstructor
public enum ContentProcessorEnum {

    FICTION("fiction", FictionContentProcessor.FCP, "书籍"),
    CATALOG("catalog", CatalogContentProcessor.CCP, "目录"),
    CHAPTER("chapter", ChapterContentProcessor.CCP, "章节"),
    ;

    private String code;
    private String strategy;
    private String desc;

}
