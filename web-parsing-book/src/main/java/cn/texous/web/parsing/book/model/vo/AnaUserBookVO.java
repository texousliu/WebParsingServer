package cn.texous.web.parsing.book.model.vo;

import lombok.Data;

import java.util.Date;

@Data
public class AnaUserBookVO {


    private String anaOriginCode;


    private String code;


    private String name;


    private String author;


    private String titlePage;


    private Integer words;


    private String summary;


    private String status;


    private Date updated;

    private Integer id;

    private String anaBookChapterCode;

}