package cn.texous.web.parsing.book.publisher;

import cn.texous.web.parsing.book.model.bo.publisher.EventBO;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/25 18:58
 */
public interface AnaSubscribe<T extends EventBO> {

    void onEvent(T object);

    String getTopic();

}
