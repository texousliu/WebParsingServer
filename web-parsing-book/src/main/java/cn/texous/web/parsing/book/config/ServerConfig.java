package cn.texous.web.parsing.book.config;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/29 11:32
 */
@Accessors(chain = true)
@Data
@Component
@ConfigurationProperties(prefix = "parsing.book.config")
public class ServerConfig {

    private String serverName;
    private String serverVersion;
    private long workId;
    private long dataCenterId;
    private String parsingEngineUrl;
    private Boolean openCache;

}
