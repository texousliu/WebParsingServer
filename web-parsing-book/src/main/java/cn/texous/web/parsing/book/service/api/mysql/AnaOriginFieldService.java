package cn.texous.web.parsing.book.service.api.mysql;

import cn.texous.web.parsing.book.config.mysql.Service;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOriginField;

import java.util.List;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-03 16:17:56
 */
public interface AnaOriginFieldService extends Service<AnaOriginField> {

    /**
     * 根据类型获取字段列表
     *
     * @param type
     * @return
     */
    List<AnaOriginField> findByType(Integer type);

}
