package cn.texous.web.parsing.book.common.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/3 17:59
 */
@Getter
@AllArgsConstructor
public enum RespTypeEnum {

    JSON("json"),
    HTML("html"),
    ;

    private String code;

}
