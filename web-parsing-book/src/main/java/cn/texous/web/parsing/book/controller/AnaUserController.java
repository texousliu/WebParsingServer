package cn.texous.web.parsing.book.controller;

import cn.texous.util.commons.constant.Result;
import cn.texous.web.parsing.book.common.constants.AnaDefaultHeaderEnum;
import cn.texous.web.parsing.book.manager.UserManager;
import cn.texous.web.parsing.book.model.dto.AnaUserDTO;
import cn.texous.web.parsing.book.model.param.AnaUserLoginParam;
import cn.texous.web.parsing.book.model.param.AnaUserRegisterParam;
import cn.texous.web.parsing.book.service.api.mysql.AnaUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * insert desciption here
 *
 * @author Showa.L
 * @since 2020/3/27 13:17
 */
@RestController
@RequestMapping("/user")
@Api(value = "/user", tags = "用户管理")
public class AnaUserController {

    @Autowired
    private AnaUserService anaUserService;

    // 注册
    @PostMapping("/register")
    @ApiOperation("用户注册")
    public Result<Boolean> retister(@RequestBody @Valid AnaUserRegisterParam param) {
        Integer id = anaUserService.register(param);
        return Result.ok(id != null && id > 0);
    }

    /**
     * TODO 需要优化到授权中心
     * @param param
     * @return
     */
    @PostMapping("/login")
    @ApiOperation("用户登录")
    public Result<String> retister(@RequestBody @Valid AnaUserLoginParam param) {
        String token = anaUserService.login(param);
        return Result.ok(token);
    }

    @PostMapping("/info")
    @ApiOperation("登录用户信息")
    public Result<AnaUserDTO> currentUserInfo() {
        AnaUserDTO anaUser = UserManager.currentUserNotNull();
        return Result.ok(anaUser);
    }

    // TODO 忘记密码 (上校验之后添加)

    // 获取用户头像列表
    @PostMapping("/headers")
    @ApiOperation("用户头像列表")
    public Result<List<Map<String, String>>> addBook() {
        // 加入头像
        List<Map<String, String>> headers = Stream.of(AnaDefaultHeaderEnum.values())
                .map(h -> {
                    Map<String, String> map = new HashMap<>();
                    map.put("name", h.name());
                    map.put("url", h.getUrl());
                    return map;
                })
                .collect(Collectors.toList());
        return Result.ok(headers);
    }

}
