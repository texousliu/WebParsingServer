package cn.texous.web.parsing.book.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel
public class ReaderInitDTO {

    @ApiModelProperty(
            notes = "书籍目录",
            example = "[]"
    )
    private List<AnaSearchChapterDTO> chapters;

    @ApiModelProperty(
            notes = "阅读进度",
            example = "20192020209838"
    )
    private String readPoint;

}
