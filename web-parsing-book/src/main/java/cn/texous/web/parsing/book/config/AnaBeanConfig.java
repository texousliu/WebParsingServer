package cn.texous.web.parsing.book.config;

import cn.texous.util.commons.util.SnowFlake;
import cn.texous.web.parsing.book.publisher.AnaPublisher;
import cn.texous.web.parsing.book.publisher.AnaSubscribe;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.Optional;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/25 11:01
 */
@Configuration
public class AnaBeanConfig {

    @Autowired
    private AnaPublisher analysisPublisher;
    @Autowired
    private ServerConfig serverConfig;

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Autowired
    public void initPublisher(Map<String, AnaSubscribe> stringAnalysisSubscribeMap) {
        Optional.ofNullable(stringAnalysisSubscribeMap.entrySet())
                .ifPresent(p -> p.forEach(item -> analysisPublisher.subscribe(item.getValue())));
    }

    @Bean
    public SnowFlake snowFlake() {
        return new SnowFlake(serverConfig.getWorkId(), serverConfig.getDataCenterId());
    }

}
