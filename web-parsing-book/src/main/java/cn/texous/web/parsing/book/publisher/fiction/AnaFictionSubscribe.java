package cn.texous.web.parsing.book.publisher.fiction;

import cn.texous.util.commons.util.SnowFlake;
import cn.texous.util.commons.util.upgrade.Optional;
import cn.texous.web.parsing.book.concurrent.AnaThreadPoolExecutor;
import cn.texous.web.parsing.book.manager.RedisManager;
import cn.texous.web.parsing.book.model.bo.publisher.AnaBookAddOrUpdateBO;
import cn.texous.web.parsing.book.model.dto.AnaSearchBookDTO;
import cn.texous.web.parsing.book.model.entity.mysql.AnaBook;
import cn.texous.web.parsing.book.publisher.AnaSubscribe;
import cn.texous.web.parsing.book.service.api.mysql.AnaBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/25 19:21
 */
@Component
public class AnaFictionSubscribe implements AnaSubscribe<AnaBookAddOrUpdateBO> {

    /**
     * topic
     */
    public static final String TOPIC = "FICTION_ADD";

    @Autowired
    private AnaBookService anaBookService;
    @Autowired
    private SnowFlake snowFlake;
    @Autowired
    private AnaThreadPoolExecutor anaThreadPoolExecutor;

    @Override
    public void onEvent(AnaBookAddOrUpdateBO object) {
        Optional.ofNullable(object)
                .map(AnaBookAddOrUpdateBO::getBooks)
                .ifPresent(bs -> bs.stream().forEach(b -> insertOrUpdate(object.getOrigin(), b)));
    }

    @Override
    public String getTopic() {
        return TOPIC;
    }

    private void insertOrUpdate(String origin, AnaSearchBookDTO b) {
        AnaBook book = anaBookService.findByUnique(origin,
                b.getName(), b.getAuthor());
        if (book == null) {
            String code = String.valueOf(snowFlake.nextId());
            AnaBook bookone = new AnaBook();
            bookone.setAnaOriginCode(origin);
            bookone.setName(b.getName());
            bookone.setAuthor(b.getAuthor());
            bookone.setTitlePage(b.getTitlePage());
            Integer words = b.getWords() == null
                    ? 0 : new BigDecimal(b.getWords()).intValue();
            bookone.setWords(words);
            bookone.setSummary(b.getSummary());
            bookone.setStatus(b.getStatus());
            bookone.setCatalogUrl(b.getCatalogUrl());
            bookone.setCode(code);
            b.setCode(code);
            // 缓存到 redis
            RedisManager.cacheBookInfo(bookone);
            anaThreadPoolExecutor.execute(() -> anaBookService.save(bookone));
        } else {
            b.setCode(book.getCode());
            book.setCatalogUrl(b.getCatalogUrl());
            book.setStatus(b.getStatus());
            Integer words = b.getWords() == null
                    ? 0 : new BigDecimal(b.getWords()).intValue();
            book.setWords(words);
            book.setTitlePage(b.getTitlePage());
            // 缓存到 redis
            RedisManager.cacheBookInfo(book);
            anaThreadPoolExecutor.execute(() -> anaBookService.update(book));
        }
    }
}
