package cn.texous.web.parsing.book.service.api.mysql;

import cn.texous.web.parsing.book.config.mysql.Service;
import cn.texous.web.parsing.book.model.dto.AnaUserBookDTO;
import cn.texous.web.parsing.book.model.entity.mysql.AnaUserBookshelf;
import cn.texous.web.parsing.book.model.param.AnaBookshelfAddParam;
import cn.texous.web.parsing.book.model.param.AnaBookshelfDeleteParam;
import cn.texous.web.parsing.book.model.param.AnaBookshelfListParam;

import java.util.List;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-11 15:27:39
 */
public interface AnaUserBookshelfService extends Service<AnaUserBookshelf> {

    Boolean addBook(AnaBookshelfAddParam param, Integer userId);

    Boolean deleteBook(AnaBookshelfDeleteParam param, Integer userId);

    List<AnaUserBookDTO> listBook(AnaBookshelfListParam param, Integer userId);

}
