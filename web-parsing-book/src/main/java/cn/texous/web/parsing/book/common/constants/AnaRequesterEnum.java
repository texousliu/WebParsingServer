package cn.texous.web.parsing.book.common.constants;

import cn.texous.web.parsing.book.strategy.search.fiction.CatalogAnaRequester;
import cn.texous.web.parsing.book.strategy.search.fiction.ChapterAnaRequester;
import cn.texous.web.parsing.book.strategy.search.fiction.FictionAnaRequester;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/7/27 16:47
 */
@Getter
@AllArgsConstructor
public enum AnaRequesterEnum {

    /**
     *
     */
    FICTION("fiction", FictionAnaRequester.FAR, "书籍"),
    /**
     *
     */
    CATALOG("catalog", CatalogAnaRequester.CAR, "目录"),
    /**
     *
     */
    CHAPTER("chapter", ChapterAnaRequester.CAR, "章节"),
    ;

    private String code;
    private String strategy;
    private String desc;

}
