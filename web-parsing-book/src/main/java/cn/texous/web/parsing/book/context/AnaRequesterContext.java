package cn.texous.web.parsing.book.context;

import cn.texous.util.commons.exception.BusinessException;
import cn.texous.web.parsing.book.common.constants.AnaRequesterEnum;
import cn.texous.web.parsing.book.common.constants.AnaResultCode;
import cn.texous.web.parsing.book.strategy.search.AnaRequester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/7/27 16:44
 */
@Component
public class AnaRequesterContext {

    private static final Map<String, AnaRequester> ANA_REQUESTER_MAP = new HashMap<>();

    @Autowired
    private void init(Map<String, AnaRequester> anaParserMap) {
        ANA_REQUESTER_MAP.clear();
        anaParserMap.forEach(ANA_REQUESTER_MAP::put);
    }

    /**
     * requester loader
     *
     * @param anaRequesterEnum enum
     * @return
     */
    public AnaRequester load(AnaRequesterEnum anaRequesterEnum) {
        return Optional.ofNullable(ANA_REQUESTER_MAP.get(anaRequesterEnum.getStrategy()))
                .orElseThrow(() -> new BusinessException(
                        AnaResultCode.FS_REQUESTER_UNSUPPORT_ERROR));
    }

}
