package cn.texous.web.parsing.book.common.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/3 17:53
 */
@Getter
@AllArgsConstructor
public enum NormalStatusEnum {

    UNAVAILABLE(0, "不可用"),
    AVAILABLE(1, "可用"),
    DELETE(2, "已删除"),
    ;

    private int code;
    private String desc;

}
