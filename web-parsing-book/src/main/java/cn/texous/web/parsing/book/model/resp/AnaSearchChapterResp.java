package cn.texous.web.parsing.book.model.resp;

import lombok.Data;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 14:01
 */
@Data
public class AnaSearchChapterResp implements Resp {

    private String code;
    private String anaBookCode;
    private String url;
    private String name;
    private String content;

}
