package cn.texous.web.parsing.book.service.impl.mysql;

import cn.texous.web.parsing.book.config.mysql.AbstractService;
import cn.texous.web.parsing.book.dao.mysql.AnaOriginDetailRequestMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOriginDetailRequest;
import cn.texous.web.parsing.book.service.api.mysql.AnaOriginDetailRequestService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-03 16:17:56
 */
@Service
public class AnaOriginDetailRequestServiceImpl
        extends AbstractService<AnaOriginDetailRequest> implements AnaOriginDetailRequestService {

    @Resource
    private AnaOriginDetailRequestMapper anaOriginDetailRequestMapper;

}
