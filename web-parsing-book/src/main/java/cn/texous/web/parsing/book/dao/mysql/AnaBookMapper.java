package cn.texous.web.parsing.book.dao.mysql;

import cn.texous.web.parsing.book.config.mysql.MyMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaBook;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AnaBookMapper extends MyMapper<AnaBook> {

    AnaBook findByUnique(@Param("origin") String origin,
                         @Param("name") String name,
                         @Param("author") String author);

    List<AnaBook> findLimit(@Param("offset") Integer offset,
                            @Param("size") Integer size);

}