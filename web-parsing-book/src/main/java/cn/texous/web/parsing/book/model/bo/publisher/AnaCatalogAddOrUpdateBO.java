package cn.texous.web.parsing.book.model.bo.publisher;

import cn.texous.web.parsing.book.model.dto.AnaSearchChapterDTO;
import lombok.Data;

import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 14:01
 */
@Data
public class AnaCatalogAddOrUpdateBO implements EventBO {

    private List<AnaSearchChapterDTO> chapters;
    private String anaBookCode;

}
