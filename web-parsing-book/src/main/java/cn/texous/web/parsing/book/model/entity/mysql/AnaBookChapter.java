package cn.texous.web.parsing.book.model.entity.mysql;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "ana_book_chapter")
public class AnaBookChapter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 所属书籍iid
     */
    @Column(name = "ana_book_code")
    private String anaBookCode;

    private String code;

    /**
     * 第几章
     */
    private Integer number;

    /**
     * 章节名字
     */
    private String name;

    /**
     * 章节字数
     */
    private Integer words;

    /**
     * 请求链接
     */
    private String url;

    private String content;

    /**
     * 创建时间
     */
    private Date created;

    /**
     * 更新时间
     */
    private Date updated;
}
