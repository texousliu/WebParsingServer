package cn.texous.web.parsing.book.model.parser.origin;

import cn.texous.util.commons.exception.BusinessException;
import cn.texous.web.parsing.book.common.constants.AnaResultCode;
import cn.texous.web.parsing.book.common.constants.OriginDetailRequestTypeEnum;
import cn.texous.web.parsing.book.common.constants.OriginDetailTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 11:16
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnaOriginParserModuleInfo implements Serializable {

    private static final long serialVersionUID = 7813391259513689400L;
    private List<AnaOriginParserRequestInfo> requestInfos;
    private List<AnaOriginParserFieldInfo> anaFieldInfos;
    private Integer weights;
    private Integer retry;
    private String dataPattern;
    // 请求类型
    private transient OriginDetailTypeEnum type;

    public AnaOriginParserRequestInfo getRequestInfoByType(OriginDetailRequestTypeEnum typeEnum) {
        return requestInfos.stream()
                .filter(m -> m.getType().equals(typeEnum))
                .findAny()
                .orElseThrow(() -> new BusinessException(
                        AnaResultCode.FS_ORIGIN_PARSER_MODULE_REQUEST_INFO_NOTFOUND));
    }

}
