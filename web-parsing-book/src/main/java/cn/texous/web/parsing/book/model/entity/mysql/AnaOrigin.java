package cn.texous.web.parsing.book.model.entity.mysql;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "ana_origin")
public class AnaOrigin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 域名
     */
    private String domain;

    /**
     * 是否使用https: 0=false,1=true
     */
    @Column(name = "open_https")
    private Integer openHttps;

    /**
     * 权重：越大权重越高
     */
    private Integer weights;

    /**
     * 重试次数
     */
    private Integer retry;

    /**
     * 时间表达式
     */
    @Column(name = "date_pattern")
    private String datePattern;

    /**
     * 数据源描述
     */
    private String description;

    /**
     * 字符编码：GBK、UTF-8
     */
    private String charset;

    /**
     * 请求结果类型：json/html
     */
    @Column(name = "resp_type")
    private String respType;

    /**
     * 开启动态解析：0=false，1=true
     */
    private Integer dyn;

    /**
     * 开启 cookies：0=false，1=true
     */
    @Column(name = "open_dookies")
    private Integer openDookies;

    /**
     * url 参数编码
     */
    @Column(name = "url_encoder")
    private String urlEncoder;

    /**
     * 自定义请求头
     */
    @Column(name = "customize_header")
    private String customizeHeader;

    /**
     * 状态：0=不可用，1=可用，2=已删除
     */
    private Integer status;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;
}
