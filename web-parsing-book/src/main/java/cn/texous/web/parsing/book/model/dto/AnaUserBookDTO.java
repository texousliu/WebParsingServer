package cn.texous.web.parsing.book.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel
public class AnaUserBookDTO {

    @ApiModelProperty(
            notes = "书籍来源",
            example = "bqb"
    )
    private String anaOriginCode;

    @ApiModelProperty(
            notes = "书籍编码",
            example = "20192020209838"
    )
    private String code;

    @ApiModelProperty(
            notes = "书籍名称",
            example = "踏星"
    )
    private String name;

    @ApiModelProperty(
            notes = "作者",
            example = "不知道"
    )
    private String author;

    @ApiModelProperty(
            notes = "封面",
            example = "https://image.test.com"
    )
    private String titlePage;

    @ApiModelProperty(
            notes = "总字数",
            example = "2340000"
    )
    private Integer words;

    @ApiModelProperty(
            notes = "摘要",
            example = "哈哈"
    )
    private String summary;

    @ApiModelProperty(
            notes = "状态：PREPARE=带获取，LOADING=连载中，FINISH=已完本",
            example = "PREPARE"
    )
    private String status;

    @ApiModelProperty(
            notes = "更新时间",
            example = "2020-02-02"
    )
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updated;

    @ApiModelProperty(
            notes = "书架编号",
            example = "123"
    )
    private Integer id;

    @ApiModelProperty(
            notes = "阅读章节编码",
            example = "20201010102903"
    )
    private String anaBookChapterCode;

}
