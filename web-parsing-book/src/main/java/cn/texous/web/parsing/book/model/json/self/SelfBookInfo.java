package cn.texous.web.parsing.book.model.json.self;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 11:16
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SelfBookInfo implements Serializable {

    private static final long serialVersionUID = 6685211489459693013L;
    private SelfHttpInfo httpInfo;
    private SelfHttpInfo nextPageInfo;
    private List<SelfFieldInfo> fieldInfos;
    private Integer weights;
    private Integer retry;
    private String dataPattern;

}
