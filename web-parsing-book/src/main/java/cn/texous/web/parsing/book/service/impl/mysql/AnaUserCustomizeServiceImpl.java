package cn.texous.web.parsing.book.service.impl.mysql;

import cn.texous.web.parsing.book.config.mysql.AbstractService;
import cn.texous.web.parsing.book.dao.mysql.AnaUserCustomizeMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaUserCustomize;
import cn.texous.web.parsing.book.service.api.mysql.AnaUserCustomizeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-11 14:20:37
 */
@Service
public class AnaUserCustomizeServiceImpl
        extends AbstractService<AnaUserCustomize> implements AnaUserCustomizeService {

    @Resource
    private AnaUserCustomizeMapper anaUserCustomizeMapper;

}
