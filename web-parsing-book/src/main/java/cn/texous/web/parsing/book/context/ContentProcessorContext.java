package cn.texous.web.parsing.book.context;

import cn.texous.util.commons.exception.BusinessException;
import cn.texous.web.parsing.book.common.constants.AnaResultCode;
import cn.texous.web.parsing.book.common.constants.ContentProcessorEnum;
import cn.texous.web.parsing.book.strategy.search.ContentProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/7/27 16:43
 */
@Component
public class ContentProcessorContext {

    private static final Map<String, ContentProcessor> CONTENT_PROCESSOR_MAP = new HashMap<>();

    @Autowired
    private void init(Map<String, ContentProcessor> contentProcessorMap) {
        CONTENT_PROCESSOR_MAP.clear();
        contentProcessorMap.forEach(CONTENT_PROCESSOR_MAP::put);
    }

    public ContentProcessor load(ContentProcessorEnum contentProcessorEnum) {
        return Optional.ofNullable(CONTENT_PROCESSOR_MAP.get(contentProcessorEnum.getStrategy()))
                .orElseThrow(() -> new BusinessException(AnaResultCode.FS_PARSER_UNSUPPORT_ERROR));
    }

}
