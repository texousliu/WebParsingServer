package cn.texous.web.parsing.book.model.parser.origin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 14:32
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnaOriginParserFieldInfo implements Serializable {

    private static final long serialVersionUID = 4516239790166125967L;
    /**
     * 是否需要转节点
     */
    private boolean changeNode;
    /**
     * xpath
     */
    @JsonProperty("xpath")
    private String xPath;
    /**
     * 节点类型
     */
    @JsonIgnore
    private Integer type;
    /**
     * 节点对应的属性
     */
    private String field;
    /**
     * 节点对应的类
     */
    @JsonIgnore
    private String className;
    /**
     * 子解析器
     */
    private List<AnaOriginParserFieldInfo> childs;

}
