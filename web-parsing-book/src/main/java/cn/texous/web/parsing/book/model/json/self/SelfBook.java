package cn.texous.web.parsing.book.model.json.self;

import lombok.Data;

import java.io.Serializable;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 11:11
 */
@Data
public class SelfBook implements Serializable {

    private static final long serialVersionUID = -8759754597019037933L;
    private String origin;
    private SelfBaseInfo baseInfo;
    private SelfBookInfo bookInfo;
    private SelfCatalogInfo catalogInfo;
    private SelfChapterInfo chapterInfo;
    private Integer weights;
    private Integer retry;
    private String dataPattern;

}
