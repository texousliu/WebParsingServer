package cn.texous.web.parsing.book.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexey Malyshev on 25.06.2018
 */
@Configuration
public class SwaggerConfig {

    private static final String ROOT_CONTROLLER = "cn.texous.web.parsing.book.controller";
    private static final String URI_APACHE = "https://www.apache.org/licenses/LICENSE-2.0";

    @Autowired
    private ServerConfig serverConfig;

    /**
     * docket api 配置
     *
     * @return
     */
    @Bean
    public Docket api() {
        List<Parameter> pars = getGlobalOperationParameters();
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(ROOT_CONTROLLER))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(pars)
                .apiInfo(apiInfo(serverConfig.getServerName(), serverConfig.getServerVersion()));
    }

    private ApiInfo apiInfo(String title, String version) {
        return new ApiInfoBuilder()
                .title("REST API " + title)
                .version("Version " + version)
                .license("Apache License Version 2.0")
                .licenseUrl(URI_APACHE)
                .build();
    }

    private List<Parameter> getGlobalOperationParameters() {
        List<Parameter> pars = new ArrayList<>();
        ParameterBuilder parameterBuilder = new ParameterBuilder();
        // header query cookie
//        parameterBuilder.name("token")
//                .description("token")
//                .modelRef(new ModelRef("String"))
//                .defaultValue("token")
//                .parameterType("header")
//                .required(true);
//        pars.add(parameterBuilder.build());
//        parameterBuilder.name("secret")
//                .description("secret")
//                .modelRef(new ModelRef("String"))
//                .defaultValue("5GcgIrI8xtuxE3_a0zEfrMn5-DVlKnbg")
//                .parameterType("header")
//                .required(true);
//        pars.add(parameterBuilder.build());
        return pars;
    }

}
