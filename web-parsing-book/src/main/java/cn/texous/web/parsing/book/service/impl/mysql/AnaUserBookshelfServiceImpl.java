package cn.texous.web.parsing.book.service.impl.mysql;

import cn.texous.util.commons.util.ConverterUtils;
import cn.texous.web.parsing.book.common.constants.NormalStatusEnum;
import cn.texous.web.parsing.book.config.mysql.AbstractService;
import cn.texous.web.parsing.book.dao.mysql.AnaUserBookshelfMapper;
import cn.texous.web.parsing.book.model.dto.AnaUserBookDTO;
import cn.texous.web.parsing.book.model.entity.mysql.AnaUserBookshelf;
import cn.texous.web.parsing.book.model.param.AnaBookshelfAddParam;
import cn.texous.web.parsing.book.model.param.AnaBookshelfDeleteParam;
import cn.texous.web.parsing.book.model.param.AnaBookshelfListParam;
import cn.texous.web.parsing.book.model.vo.AnaUserBookVO;
import cn.texous.web.parsing.book.service.api.mysql.AnaUserBookshelfService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-11 15:27:39
 */
@Service
public class AnaUserBookshelfServiceImpl
        extends AbstractService<AnaUserBookshelf> implements AnaUserBookshelfService {

    @Resource
    private AnaUserBookshelfMapper anaUserBookshelfMapper;

    @Override
    public Boolean addBook(AnaBookshelfAddParam param, Integer userId) {
        AnaUserBookshelf bookshelf = anaUserBookshelfMapper.findByUnique(userId, param.getAnaBookCode());

        if (bookshelf == null) {
            bookshelf = new AnaUserBookshelf();
            bookshelf.setAnaUserId(userId);
            bookshelf.setAnaBookCode(param.getAnaBookCode());
            bookshelf.setAnaBookChapterCode(param.getAnaBookChapterCode() == null
                    ? "" : param.getAnaBookChapterCode());
            bookshelf.setStatus(NormalStatusEnum.AVAILABLE.getCode());
            anaUserBookshelfMapper.insertSelective(bookshelf);
        } else {
            bookshelf.setAnaBookChapterCode(param.getAnaBookChapterCode() == null
                    ? "" : param.getAnaBookChapterCode());
            bookshelf.setStatus(NormalStatusEnum.AVAILABLE.getCode());
            anaUserBookshelfMapper.updateByPrimaryKeySelective(bookshelf);
        }
        return true;
    }

    @Override
    public Boolean deleteBook(AnaBookshelfDeleteParam param, Integer userId) {
        AnaUserBookshelf bookshelf = anaUserBookshelfMapper.findByUnique(userId, param.getAnaBookCode());
        if (bookshelf != null) {
            bookshelf.setStatus(NormalStatusEnum.DELETE.getCode());
            anaUserBookshelfMapper.updateByPrimaryKeySelective(bookshelf);
        }
        return true;
    }

    @Override
    public List<AnaUserBookDTO> listBook(AnaBookshelfListParam param, Integer userId) {
        Map<String, Object> params = ConverterUtils.transBean2Map(param);
        params.put("userId", userId);
        List<AnaUserBookVO> bookVOS = anaUserBookshelfMapper.findByParam(params);
        return ConverterUtils.convert(AnaUserBookDTO.class, bookVOS);
    }
}
