package cn.texous.web.parsing.book.common.constants;

import cn.texous.util.commons.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/3 18:01
 */
@Getter
@AllArgsConstructor
public enum OriginDetailTypeEnum {

    FICTION(1, "书籍"),
    CATALOG(2, "目录"),
    CHAPTER(3, "章节"),
    ;

    private int code;
    private String desc;

    public static OriginDetailTypeEnum loadByCode(int code) {
        return Stream.of(OriginDetailTypeEnum.values())
                .filter(r -> r.getCode() == code)
                .findAny()
                .orElseThrow(() -> new BusinessException(
                        AnaResultCode.FS_ORIGIN_DETAIL_TYPE_ERROR));
    }

}
