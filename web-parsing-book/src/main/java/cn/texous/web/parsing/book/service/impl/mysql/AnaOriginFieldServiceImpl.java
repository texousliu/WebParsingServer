package cn.texous.web.parsing.book.service.impl.mysql;

import cn.texous.web.parsing.book.config.mysql.AbstractService;
import cn.texous.web.parsing.book.dao.mysql.AnaOriginFieldMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOriginField;
import cn.texous.web.parsing.book.service.api.mysql.AnaOriginFieldService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-03 16:17:56
 */
@Service
public class AnaOriginFieldServiceImpl
        extends AbstractService<AnaOriginField> implements AnaOriginFieldService {

    @Resource
    private AnaOriginFieldMapper anaOriginFieldMapper;

    @Override
    public List<AnaOriginField> findByType(Integer type) {
        return anaOriginFieldMapper.findByType(type);
    }


}
