package cn.texous.web.parsing.book.model.entity.mysql;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "ana_user_bookshelf")
public class AnaUserBookshelf {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户id
     */
    @Column(name = "ana_user_id")
    private Integer anaUserId;

    /**
     * 书籍唯一编码
     */
    @Column(name = "ana_book_code")
    private String anaBookCode;

    /**
     * 当前阅读章节唯一编码
     */
    @Column(name = "ana_book_chapter_code")
    private String anaBookChapterCode;

    private Integer status;

    private Date created;

    private Date updated;
}
