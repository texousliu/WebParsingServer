package cn.texous.web.parsing.book.common.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/6 16:59
 */
@Getter
@AllArgsConstructor
public enum SearchTypeEnum {

    ALL(0, "全部"),
    BOOK_NAME(1, "书名"),
    BOOK_AUTHOR(2, "作者"),
    BOOK_LEADER(3, "主角"),
    ;

    private int code;
    private String desc;

}
