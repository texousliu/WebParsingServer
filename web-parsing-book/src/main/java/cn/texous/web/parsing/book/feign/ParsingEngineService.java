package cn.texous.web.parsing.book.feign;

import cn.texous.util.commons.constant.Result;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/13 17:57
 */
public interface ParsingEngineService {

    @POST("/api/parsing")
    Call<Result<String>> parsing(@Body AnaParsingInfo parsingInfo);

}
