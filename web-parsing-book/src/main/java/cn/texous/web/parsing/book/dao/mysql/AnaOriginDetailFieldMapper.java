package cn.texous.web.parsing.book.dao.mysql;

import cn.texous.web.parsing.book.config.mysql.MyMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOriginDetailField;

import java.util.List;
import java.util.Map;

public interface AnaOriginDetailFieldMapper extends MyMapper<AnaOriginDetailField> {

    List<AnaOriginDetailField> selectByParam(Map<String, Object> params);

}
