package cn.texous.web.parsing.book.strategy.search;

import cn.texous.util.commons.constant.Result;
import cn.texous.web.parsing.book.feign.AnaParsingInfo;
import cn.texous.web.parsing.book.model.param.Param;
import cn.texous.web.parsing.book.model.parser.origin.AnaOriginParser;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 10:21
 */
public interface AnaRequester<P extends Param> {
    /**
     * 获取请求信息
     *
     * @param param        请求参数
     * @param originParser 数据源配置
     * @return 返回获取的请求信息
     */
    AnaParsingInfo getRequestInfo(P param, AnaOriginParser originParser);

    /**
     * 发起请求
     *
     * @param info info
     * @return 返回请求结果
     */
    Result<String> execute(AnaParsingInfo info);

}
