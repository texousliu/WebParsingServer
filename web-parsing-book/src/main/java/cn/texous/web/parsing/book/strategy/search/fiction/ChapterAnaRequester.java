package cn.texous.web.parsing.book.strategy.search.fiction;

import cn.texous.util.commons.exception.BusinessException;
import cn.texous.util.commons.util.ConverterUtils;
import cn.texous.web.parsing.book.common.constants.AnaResultCode;
import cn.texous.web.parsing.book.common.constants.OriginDetailRequestTypeEnum;
import cn.texous.web.parsing.book.common.constants.OriginDetailTypeEnum;
import cn.texous.web.parsing.book.feign.AnaParsingInfo;
import cn.texous.web.parsing.book.model.entity.mysql.AnaBookChapter;
import cn.texous.web.parsing.book.model.param.AnaSearchChapterParam;
import cn.texous.web.parsing.book.model.parser.AnaRequestInfo;
import cn.texous.web.parsing.book.model.parser.origin.AnaOriginParser;
import cn.texous.web.parsing.book.model.parser.origin.AnaOriginParserModuleInfo;
import cn.texous.web.parsing.book.model.parser.origin.AnaOriginParserRequestInfo;
import cn.texous.web.parsing.book.service.api.mysql.AnaBookChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 10:56
 */
@Component(ChapterAnaRequester.CAR)
public class ChapterAnaRequester extends AbstractAnaRequester<AnaSearchChapterParam> {

    /**
     * bean name
     */
    public static final String CAR = "chapterAnaRequester";

    @Override
    public AnaParsingInfo getRequestInfo(AnaSearchChapterParam searchParam, AnaOriginParser originParser) {
        AnaOriginParserModuleInfo moduleInfo = originParser
                .getModuleInfoByType(OriginDetailTypeEnum.CHAPTER);
        AnaOriginParserRequestInfo parserRequestInfo = moduleInfo
                .getRequestInfoByType(OriginDetailRequestTypeEnum.NORMAL);

        AnaRequestInfo requestInfo = ConverterUtils.convert(AnaRequestInfo.class, parserRequestInfo);

        requestInfo.setUrl(searchParam.getUrl());
        addHttpCharset(parserRequestInfo, requestInfo, originParser.getBaseInfo());

        AnaParsingInfo parsingInfo = new AnaParsingInfo();
        parsingInfo.setRespType(originParser.getBaseInfo().getRespType());
        parsingInfo.setRequestInfo(requestInfo);
        parsingInfo.setParsingFieldInfos(moduleInfo.getAnaFieldInfos());

        return parsingInfo;
    }

}
