package cn.texous.web.parsing.book.model.bo.publisher;

import lombok.Data;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 14:01
 */
@Data
public class AnaChapterAddOrUpdateBO implements EventBO {

    private String anaBookChapterCode;
    private String name;
    private String content;

}
