/**
 * 自己的书源配置文件导入解析实体类
 *
 * @author Showa.L
 * @since 2020/3/10 16:50
 */
package cn.texous.web.parsing.book.model.json.self;