package cn.texous.web.parsing.book.config;

import cn.texous.web.parsing.book.feign.ParsingEngineService;
import org.hibernate.validator.HibernateValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/9/1 12:37
 */
@Configuration
public class ServerBeanConfig {

    @Autowired
    private ServerConfig serverConfig;

    /**
     * 配置 controller 参数校验
     *
     * @return
     */
    @Bean
    public Validator validator() {
        ValidatorFactory factory = Validation.byProvider(HibernateValidator.class)
                .configure()
                // 将fail_fast设置为true即可，如果想验证全部，则设置为false或者取消配置即可
                .addProperty("hibernate.validator.fail_fast", "true")
                .buildValidatorFactory();
        return factory.getValidator();
    }

    @Bean
    public ParsingEngineService parsingEngineService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serverConfig.getParsingEngineUrl())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        return retrofit.create(ParsingEngineService.class);
    }

}
