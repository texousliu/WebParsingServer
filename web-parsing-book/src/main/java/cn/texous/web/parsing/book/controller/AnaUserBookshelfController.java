package cn.texous.web.parsing.book.controller;

import cn.texous.util.commons.constant.Result;
import cn.texous.web.parsing.book.manager.UserManager;
import cn.texous.web.parsing.book.model.dto.AnaUserBookDTO;
import cn.texous.web.parsing.book.model.param.AnaBookshelfAddParam;
import cn.texous.web.parsing.book.model.param.AnaBookshelfDeleteParam;
import cn.texous.web.parsing.book.model.param.AnaBookshelfListParam;
import cn.texous.web.parsing.book.model.param.AnaBookshelfReadPointParam;
import cn.texous.web.parsing.book.service.api.mysql.AnaUserBookService;
import cn.texous.web.parsing.book.service.api.mysql.AnaUserBookshelfService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/11 17:09
 */
@RestController
@RequestMapping("/bookshelf")
@Api(value = "/bookshelf", tags = "书架管理")
public class AnaUserBookshelfController {

    @Autowired
    private AnaUserBookshelfService anaUserBookshelfService;
    @Autowired
    private AnaUserBookService anaUserBookService;

    /**
     * TODO 需要开启新线程去将网络资源导入到本地
     * @param param
     * @return
     */
    @PostMapping("/book/add")
    @ApiOperation("添加书籍")
    public Result<Boolean> addBook(@RequestBody @Valid AnaBookshelfAddParam param) {
        Integer userId = UserManager.currentUserIdNotNull();
        Boolean addResult = anaUserBookshelfService.addBook(param, userId);
        return Result.ok(addResult);
    }

    @PostMapping("/book/delete")
    @ApiOperation("删除书籍")
    public Result<Boolean> deleteBook(@RequestBody @Valid AnaBookshelfDeleteParam param) {
        Integer userId = UserManager.currentUserIdNotNull();
        Boolean addResult = anaUserBookshelfService.deleteBook(param, userId);
        return Result.ok(addResult);
    }

    @PostMapping("/book/list")
    @ApiOperation("书籍列表")
    public Result<List<AnaUserBookDTO>> listBook(@RequestBody @Valid AnaBookshelfListParam param) {
        Integer userId = UserManager.currentUserIdNotNull();
        List<AnaUserBookDTO> books = anaUserBookshelfService.listBook(param, userId);
        return Result.ok(books);
    }

    @PostMapping("/book/point/read")
    @ApiOperation("获取用户阅读进度")
    public Result<String> readPoint(@RequestBody @Valid AnaBookshelfReadPointParam param) {
        Integer userId = UserManager.currentUserId();
        String readPoint = anaUserBookService.getReadPoint(param, userId);
        return Result.ok(readPoint);
    }

}
