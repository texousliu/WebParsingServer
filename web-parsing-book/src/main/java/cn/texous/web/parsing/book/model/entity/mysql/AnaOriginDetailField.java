package cn.texous.web.parsing.book.model.entity.mysql;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "ana_origin_detail_field")
public class AnaOriginDetailField {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ana_origin_detail_id")
    private Integer anaOriginDetailId;

    /**
     * 父类id，没有时为0，默认为0
     */
    @Column(name = "parent_id")
    private Integer parentId;

    /**
     * 是否时叶子节点：0=否，1=是。默认1
     */
    @Column(name = "is_leaf")
    private Integer isLeaf;

    /**
     * 字段名
     */
    private String field;

    /**
     * 解析表达式：json时使用jsonpath，xml时使用 xpath
     */
    @Column(name = "x_path")
    private String xPath;

    /**
     * 类名，预留字段
     */
    @Column(name = "class_name")
    private String className;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;
}
