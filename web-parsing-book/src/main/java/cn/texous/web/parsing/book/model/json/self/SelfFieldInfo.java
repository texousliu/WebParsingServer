package cn.texous.web.parsing.book.model.json.self;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 14:32
 */
@Data
public class SelfFieldInfo implements Serializable {

    private static final long serialVersionUID = -5441826872626275775L;
    /**
     * 是否需要转节点
     */
    private boolean changeNode;
    /**
     * xpath
     */
    private String xPath;
    /**
     * 节点类型
     */
    private String type;
    /**
     * 节点对应的属性
     */
    private String field;
    /**
     * 节点对应的类
     */
    private String className;
    /**
     * 子解析器
     */
    private List<SelfFieldInfo> childs;

}
