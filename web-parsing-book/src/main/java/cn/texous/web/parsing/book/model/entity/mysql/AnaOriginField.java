package cn.texous.web.parsing.book.model.entity.mysql;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "ana_origin_field")
public class AnaOriginField {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 字段
     */
    private String field;

    /**
     * 类型：1=书籍，2=目录，3=章节
     */
    private Integer type;

    /**
     * 字段描述
     */
    private String description;

    /**
     * 父节点id
     */
    @Column(name = "parent_id")
    private Integer parentId;

    /**
     * 是否为叶子节点：0=否，1=是
     */
    @Column(name = "is_leaf")
    private Integer isLeaf;

    @Column(name = "create_time")
    private Date createTime;
}
