package cn.texous.web.parsing.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * application main
 *
 * @author leven
 * @since
 */
@SpringBootApplication
@MapperScan("cn.texous.web.parsing.book.dao.mysql")
@EnableSwagger2
public class WebParsingBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebParsingBookApplication.class, args);
    }

}
