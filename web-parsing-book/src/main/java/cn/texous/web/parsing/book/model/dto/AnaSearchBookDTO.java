package cn.texous.web.parsing.book.model.dto;

import lombok.Data;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 11:08
 */
@Data
public class AnaSearchBookDTO {

    private String code;
    private String name; //  "",
    private String author; //  "",
    private String titlePage; //  "",
    private String updated; //  "",
    private String type; //  "",
    private String status; //  "",
    private String words; //  "",
    private String summary; //  "",
    private String retentionRate; //  "",
    private String labels; //  "",
    private String catalogUrl; //  "",
    private String lastCapterTitle; //  ""
    private String lastCapterUrl; //  ""

}
