package cn.texous.web.parsing.book.model.entity.mysql;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "ana_origin_detail_request")
public class AnaOriginDetailRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ana_origin_detail_id")
    private Integer anaOriginDetailId;

    /**
     * 类型：1=常规请求，2=下一页，3=上一页
     */
    private Integer type;

    /**
     * 请求方式
     */
    private String method;

    /**
     * 请求链接
     */
    private String url;

    /**
     * 请求参数
     */
    private String param;

    /**
     * url 字符编码
     */
    @Column(name = "url_encoder")
    private String urlEncoder;

    /**
     * 输入字符编码
     */
    @Column(name = "in_charset")
    private String inCharset;

    /**
     * 输出字符编码
     */
    @Column(name = "out_charset")
    private String outCharset;

    /**
     * 状态：0=不可用，1=可用，2=已删除
     */
    private Integer status;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;
}
