package cn.texous.web.parsing.book.model.parser.origin;

import cn.texous.web.parsing.book.common.constants.OriginDetailRequestTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 11:17
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnaOriginParserRequestInfo implements Serializable {

    private static final long serialVersionUID = 1687301825672862046L;
    // 请求方式： GET POST
    private String method;
    // 请求链接：
    private String url;
    // 请求参数
    private Map<String, String> params;
    // 请求参数是否 url编码
    private String urlEncoder;
    // 输入流编码
    private String inCharset;
    // 输出流编码
    private String outCharset;
    // 请求类型
    private transient OriginDetailRequestTypeEnum type;

}
