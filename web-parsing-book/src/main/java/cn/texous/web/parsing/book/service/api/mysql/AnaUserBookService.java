package cn.texous.web.parsing.book.service.api.mysql;

import cn.texous.web.parsing.book.config.mysql.Service;
import cn.texous.web.parsing.book.model.entity.mysql.AnaUserBook;
import cn.texous.web.parsing.book.model.param.AnaBookshelfReadPointParam;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-11 14:20:37
 */
public interface AnaUserBookService extends Service<AnaUserBook> {

    Integer insertOrUpdate(AnaUserBook userBook);

    Integer insertOrUpdate(String bookCode, String chapterCode, Integer userId);

    String getReadPoint(AnaBookshelfReadPointParam param, Integer userId);

}
