package cn.texous.web.parsing.book.model.param;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/24 17:13
 */
@Data
@ApiModel
public class AnaSearchChapterParam implements Param, Serializable {

    private static final long serialVersionUID = 8298279447896216352L;
    @ApiModelProperty(
            notes = "章节 id: 由目录检索获取",
            example = "123werfasdfa"
    )
    private String anaBookChapterCode;

    @ApiModelProperty(
            notes = "搜索源: 具体见字典接口 origin",
            example = "bqb"
    )
    private String origin;

    @JsonIgnore
    private transient String url;
}
