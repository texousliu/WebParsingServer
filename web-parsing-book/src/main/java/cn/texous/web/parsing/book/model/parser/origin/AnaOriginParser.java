package cn.texous.web.parsing.book.model.parser.origin;

import cn.texous.util.commons.exception.BusinessException;
import cn.texous.web.parsing.book.common.constants.AnaResultCode;
import cn.texous.web.parsing.book.common.constants.OriginDetailTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 11:11
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnaOriginParser implements Serializable {

    private static final long serialVersionUID = -1515032127408571234L;
    private String origin;
    private AnaOriginParserBaseInfo baseInfo;
    private List<AnaOriginParserModuleInfo> moduleInfos;
    private Integer weights;
    private Integer retry;
    private String dataPattern;

    public AnaOriginParserModuleInfo getModuleInfoByType(OriginDetailTypeEnum typeEnum) {
        return moduleInfos.stream()
                .filter(m -> m.getType().equals(typeEnum))
                .findAny()
                .orElseThrow(() -> new BusinessException(
                        AnaResultCode.FS_ORIGIN_PARSER_MODULE_INFO_NOTFOUND));
    }

}
