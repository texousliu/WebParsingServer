package cn.texous.web.parsing.book.dao.mysql;

import cn.texous.web.parsing.book.config.mysql.MyMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaBookChapter;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AnaBookChapterMapper extends MyMapper<AnaBookChapter> {

    AnaBookChapter findByUnique(@Param("bookCode") String bookCode,
                                @Param("name") String name);

    List<AnaBookChapter> findByBookCode(@Param("bookCode") String bookCode);

    AnaBookChapter findByCode(@Param("code") String code);

    Integer insertIgnore(@Param("chapters") List<AnaBookChapter> chapters);

    AnaBookChapter getFirstChapter(@Param("bookCode") String bookCode);

}
