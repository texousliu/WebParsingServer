package cn.texous.web.parsing.book.controller.open;

import cn.texous.util.commons.constant.Result;
import cn.texous.util.commons.util.ConverterUtils;
import cn.texous.web.parsing.book.common.constants.AnaDefaultHeaderEnum;
import cn.texous.web.parsing.book.common.constants.SearchTypeEnum;
import cn.texous.web.parsing.book.service.api.mysql.AnaOriginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/6 17:03
 */
@RestController
@RequestMapping("/dict")
@Api(value = "/dict", tags = "字典管理")
public class DictionaryController {

    @Autowired
    private AnaOriginService anaOriginService;

    @PostMapping("/all")
    @ApiOperation("字典集合")
    public Result<Map<String, Object>> allDict() {
        Map<String, Object> dictionary = new HashMap<>();

        // 加入 search type
        Object searchType = Stream.of(SearchTypeEnum.values())
                .map(ConverterUtils::transBean2Map)
                .collect(Collectors.toList());
        dictionary.put("searchType", searchType);

        // 加入 origin
        dictionary.put("origin", anaOriginService.originList());

        return Result.ok(dictionary);
    }

}
