package cn.texous.web.parsing.book.model.vo;

import lombok.Data;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/6 17:20
 */
@Data
public class AnaOriginDictVO {

    private String code;
    private String desc;

}
