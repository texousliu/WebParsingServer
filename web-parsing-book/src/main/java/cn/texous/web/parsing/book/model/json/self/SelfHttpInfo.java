package cn.texous.web.parsing.book.model.json.self;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 11:17
 */
@Data
public class SelfHttpInfo implements Serializable {

    private static final long serialVersionUID = 5823672206901566060L;
    // 请求方式： GET POST
    private String method;
    // 请求链接：
    private String url;
    // 请求参数
    private Map<String, String> params;
    // 请求参数是否 url编码
    private String urlEncoder;
    // 输入流编码
    private String inCharset;
    // 输出流编码
    private String outCharset;

}
