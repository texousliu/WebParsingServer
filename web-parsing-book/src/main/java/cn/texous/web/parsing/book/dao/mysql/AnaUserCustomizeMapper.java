package cn.texous.web.parsing.book.dao.mysql;

import cn.texous.web.parsing.book.config.mysql.MyMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaUserCustomize;

public interface AnaUserCustomizeMapper extends MyMapper<AnaUserCustomize> {
}