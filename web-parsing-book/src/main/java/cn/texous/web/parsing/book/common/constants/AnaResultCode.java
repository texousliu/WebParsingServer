package cn.texous.web.parsing.book.common.constants;

import cn.texous.util.commons.constant.Code;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/24 18:39
 */
@Getter
@AllArgsConstructor
public enum AnaResultCode implements Code {

    FS_UNSUPPORT_ERROR(10001, "不支持的检索类型"),
    FS_ORIGIN_UNSUPPORT_ERROR(10002, "不支持的数据源"),
    FS_PARSER_UNSUPPORT_ERROR(10003, "不支持的解析方式"),
    FS_REQUESTER_UNSUPPORT_ERROR(10004, "不支持的请求方式"),
    FS_FICTION_NOTFIND(10005, "书籍获取失败"),
    FS_CHAPTER_NOTFIND(10006, "章节获取失败"),
    FS_ORIGIN_DETAIL_TYPE_ERROR(10007, "不支持的数据源模块类型"),
    FS_ORIGIN_DETAIL_REQUEST_TYPE_ERROR(10008, "不支持的数据源模块请求类型"),

    FS_ORIGIN_PARSER_MODULE_INFO_NOTFOUND(10009, "数据源模块获取失败"),
    FS_ORIGIN_PARSER_MODULE_REQUEST_INFO_NOTFOUND(10010, "数据源模块请求信息获取失败"),


    REMOTE_PARSING_ENGINE_ERROR(11000, "调用解析引擎失败"),

    FS_BOOK_CHAPTER_NOT_EXISTS_ERROR(12000, "章节不存在，请稍后再试"),
    ANA_USER_EXISTS_ERROR(12001, "用户已经存在"),
    ANA_USER_NOT_EXISTS_ERROR(12002, "用户不存在"),
    USERNAME_OR_PWD__ERROR(12003, "用户名或密码错误"),
    ;

    private int code;
    private String message;

}
