package cn.texous.web.parsing.book.manager;


import cn.texous.util.commons.constant.ResultCode;
import cn.texous.util.commons.exception.BusinessException;
import cn.texous.web.parsing.book.common.constants.RedisKey;
import cn.texous.web.parsing.book.common.utils.RedisClient;
import cn.texous.web.parsing.book.model.dto.AnaUserDTO;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * 登录用户信息管理
 *
 * @author Showa.L
 * @since 2020-02-25 14:41:12
 */
@Slf4j
@Component
public class UserManager {

    /**
     * 获取当前用户的id
     *
     * @return
     */
    public static Integer currentUserId() {
        return Optional.ofNullable(currentUser())
                .map(AnaUserDTO::getId)
                .orElse(null);
    }

    /**
     * 获取当前用户的 id
     *
     * @return
     */
    public static Integer currentUserIdNotNull() {
        return Optional.ofNullable(currentUserId())
                .orElseThrow(() -> new BusinessException(ResultCode.UNAUTHORIZED));
    }

    /**
     * 解析 secret 中的 username
     *
     * @return
     */
    public static String currentUserName() {
        return Optional.ofNullable(currentUser())
                .map(AnaUserDTO::getUsername)
                .orElse(null);
    }

    /**
     * 获取当前登录用户的 token
     *
     * @return
     */
    public static String currentToken() {
        return currentHeadValue("token");
    }

    /**
     * 获取当前用户信息
     *
     * @return
     */
    public static AnaUserDTO currentUserNotNull() {
        return Optional.ofNullable(currentUser())
                .orElseThrow(() -> new BusinessException(ResultCode.UNAUTHORIZED));
    }

    /**
     * 获取当前用户信息
     * TODO 需要从 redis 或者别的方式获取
     *
     * @return
     */
    public static AnaUserDTO currentUser() {
        if (false) {
            AnaUserDTO anaUser = new AnaUserDTO();
            anaUser.setId(1);
            return anaUser;
        }
        return Optional.ofNullable(currentToken())
                .map(UserManager::tokenKey)
                .map(RedisClient::get)
                .map(us -> JSON.parseObject(us, AnaUserDTO.class))
                .orElse(null);
    }

    /**
     * 获取当前请求
     *
     * @return
     */
    public static HttpServletRequest currentRequest() {
        return ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes())
                .getRequest();
    }

    /**
     * 获取请求头信息
     *
     * @param key key
     * @return
     */
    public static String currentHeadValue(String key) {
        return Optional.ofNullable(key)
                .map(k -> currentRequest().getHeader(k))
                .orElse(null);
    }

    public static String tokenKey(String token) {
        return RedisKey.LOGIN_USER_INFO_KEY_PREFIX.getPrefix() + token;
    }

}
