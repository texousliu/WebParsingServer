package cn.texous.web.parsing.book.service.api.mysql;

import cn.texous.web.parsing.book.config.mysql.Service;
import cn.texous.web.parsing.book.model.entity.mysql.AnaUser;
import cn.texous.web.parsing.book.model.param.AnaUserLoginParam;
import cn.texous.web.parsing.book.model.param.AnaUserRegisterParam;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-11 14:20:37
 */
public interface AnaUserService extends Service<AnaUser> {

    Integer register(AnaUserRegisterParam param);

    String login(AnaUserLoginParam param);

}
