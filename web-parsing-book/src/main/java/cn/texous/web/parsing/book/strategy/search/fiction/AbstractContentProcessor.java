package cn.texous.web.parsing.book.strategy.search.fiction;

import cn.texous.util.commons.constant.Result;
import cn.texous.util.commons.constant.ResultCode;
import cn.texous.util.commons.util.GsonUtils;
import cn.texous.web.parsing.book.model.resp.Resp;
import cn.texous.web.parsing.book.publisher.AnaPublisher;
import cn.texous.web.parsing.book.strategy.search.ContentProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 10:44
 */
@Slf4j
public abstract class AbstractContentProcessor<R extends Resp> implements ContentProcessor<R> {

    @Autowired
    protected AnaPublisher anaPublisher;

    protected boolean validateResult(Result<String> result) {
        if (result == null || result.getCode() != ResultCode.SUCCESS.getCode()) {
            log.info("【失败】解析失败：result={}", GsonUtils.toJson(result));
            return false;
        }
        return true;
    }

}
