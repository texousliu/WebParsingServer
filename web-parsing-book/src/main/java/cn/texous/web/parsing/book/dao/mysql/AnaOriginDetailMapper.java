package cn.texous.web.parsing.book.dao.mysql;

import cn.texous.web.parsing.book.config.mysql.MyMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOriginDetail;

import java.util.List;
import java.util.Map;

public interface AnaOriginDetailMapper extends MyMapper<AnaOriginDetail> {

    List<AnaOriginDetail> selectByParam(Map<String, Object> params);

}