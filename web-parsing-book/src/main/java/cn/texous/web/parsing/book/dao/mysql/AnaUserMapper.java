package cn.texous.web.parsing.book.dao.mysql;

import cn.texous.web.parsing.book.config.mysql.MyMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaUser;
import org.apache.ibatis.annotations.Param;

public interface AnaUserMapper extends MyMapper<AnaUser> {

    Integer existsWithPhoneNumber(@Param("phoneNumber") String phoneNumber);

    AnaUser selectByPhoneNumber(@Param("phoneNumber") String phoneNumber);

}
