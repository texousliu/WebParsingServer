package cn.texous.web.parsing.book.controller;

import cn.texous.web.parsing.book.service.api.mysql.AnaBookChapterService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * insert desciption here
 *
 * @author Showa.L
 * @since 2020/3/27 13:17
 */
@RestController
@RequestMapping("/reader")
@Api(value = "/reader", tags = "阅读器管理")
public class AnaReaderController {

    @Autowired
    private AnaBookChapterService anaBookChapterService;

//    @PostMapping("/init")
//    @ApiOperation("初始化信息")
//    public Result<ReaderInitDTO> addBook(@RequestBody @Valid AnaReaderInitParam param) {
//        Integer userId = UserManager.currentUserIdNotNull();
//        Boolean addResult = anaUserBookshelfService.addBook(param, userId);
//        return Result.ok(addResult);
//    }
//
}
