package cn.texous.web.parsing.book.model.entity.mysql;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "ana_user_customize")
public class AnaUserCustomize {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 字体
     */
    private String font;

    /**
     * 字体颜色
     */
    @Column(name = "font_color")
    private String fontColor;

    /**
     * 字体大小
     */
    @Column(name = "font_size")
    private Integer fontSize;

    /**
     * 背景颜色
     */
    private String background;

    /**
     * 行高
     */
    @Column(name = "line_height")
    private Integer lineHeight;

    /**
     * 创建时间
     */
    private Date created;

    /**
     * 更新时间
     */
    private Date updated;
}
