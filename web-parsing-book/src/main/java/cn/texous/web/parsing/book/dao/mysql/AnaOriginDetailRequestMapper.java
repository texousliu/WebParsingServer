package cn.texous.web.parsing.book.dao.mysql;

import cn.texous.web.parsing.book.config.mysql.MyMapper;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOriginDetailRequest;

import java.util.List;
import java.util.Map;

public interface AnaOriginDetailRequestMapper extends MyMapper<AnaOriginDetailRequest> {

    List<AnaOriginDetailRequest> selectByParam(Map<String, Object> params);

}