package cn.texous.web.parsing.book.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020/3/11 17:12
 */
@Data
@ApiModel
public class AnaReaderInitParam {

    @ApiModelProperty(
            required = true,
            notes = "书籍编码",
            example = "20122332231231312"
    )
    @NotNull(message = "书籍编码不能为空")
    private String anaBookCode;

}
