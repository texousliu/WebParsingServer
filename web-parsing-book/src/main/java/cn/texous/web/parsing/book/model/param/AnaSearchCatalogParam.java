package cn.texous.web.parsing.book.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/24 17:13
 */
@Data
@ApiModel
public class AnaSearchCatalogParam implements Param, Serializable {

    private static final long serialVersionUID = 8298279447896216352L;

    @ApiModelProperty(
            required = true,
            notes = "书籍CODE，由书籍搜索结果获取",
            example = "dfadfadfasdf"
    )
    @NotNull(message = "ana book code is required")
    private String anaBookCode;

    @ApiModelProperty(
            notes = "搜索源: 具体见字典接口 origin",
            example = "bqb"
    )
    private String origin;

}
