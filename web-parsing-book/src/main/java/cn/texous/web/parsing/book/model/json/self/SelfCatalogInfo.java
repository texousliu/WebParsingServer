package cn.texous.web.parsing.book.model.json.self;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/7/26 11:16
 */
@Data
public class SelfCatalogInfo implements Serializable {

    private static final long serialVersionUID = -829899343350701620L;
    private SelfHttpInfo httpInfo;
    private SelfHttpInfo nextPageInfo;
    private List<SelfFieldInfo> fieldInfos;
    private Integer weights;
    private Integer retry;
    private String dataPattern;

}
