package cn.texous.web.parsing.book.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class AnaUserDTO {

    @ApiModelProperty(
            notes = "id",
            example = "123"
    )
    private Integer id;

    @ApiModelProperty(
            notes = "用户名",
            example = "123"
    )
    private String username;

    @ApiModelProperty(
            notes = "昵称",
            example = "nick"
    )
    private String nickname;

    @ApiModelProperty(
            notes = "头像：通过 用户头像列表 接口获取",
            example = "https://leven-test-bucket.nos-eastchina1.126.net/reader/bird.png"
    )
    private String headImg;

    @ApiModelProperty(
            notes = "手机号",
            example = "18900000000"
    )
    private String phoneNumber;

}
