package cn.texous.web.parsing.book.service.api.mysql;

import cn.texous.web.parsing.book.config.mysql.Service;
import cn.texous.web.parsing.book.model.entity.mysql.AnaOrigin;
import cn.texous.web.parsing.book.model.parser.origin.AnaOriginParser;
import cn.texous.web.parsing.book.model.vo.AnaOriginDictVO;

import java.util.List;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2020-03-03 16:17:56
 */
public interface AnaOriginService extends Service<AnaOrigin> {

    List<AnaOriginParser> buildOrigin();

    List<AnaOriginDictVO> originList();

}
