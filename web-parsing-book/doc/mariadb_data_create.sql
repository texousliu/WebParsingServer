
-- 初始化字段表
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(1, "nextPageUrl", 1, "书籍下一页", 0, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(2, "books", 1, "书籍列表", 0, 0);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(3, "name", 1, "名称", 2, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(4, "author", 1, "书籍作者", 2, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(5, "summary", 1, "书籍简介", 2, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(6, "titlePage", 1, "书籍封面", 2, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(7, "type", 1, "书籍类型", 2, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(8, "updated", 1, "书籍更新时间", 2, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(9, "status", 1, "书籍状态", 2, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(10, "words", 1, "书籍字数", 2, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(11, "catalogUrl", 1, "书籍目录连接", 2, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(12, "lastCapterTitle", 1, "书籍最新章节标题", 2, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(13, "lastCapterUrl", 1, "书籍最新章节链接", 2, 1);

INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(14, "nextPageUrl", 2, "目录下一页链接", 0, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(15, "chapters", 2, "章节列表", 0, 0);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(16, "name", 2, "章节标题", 15, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(17, "url", 2, "章节链接", 15, 1);

INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(18, "nextPageUrl", 3, "下一页链接", 0, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(19, "name", 3, "章节标题", 0, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(20, "content", 3, "章节内容", 0, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(21, "url", 3, "目录链接", 0, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(22, "nextChapterUrl", 3, "下一章链接", 0, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(23, "preChapterUrl", 3, "上一章链接", 0, 1);
INSERT INTO ana_origin_field (id, field, type, description, parent_id, is_leaf)
VALUES
(24, "prePageUrl", 3, "上一页链接", 0, 1);

INSERT INTO `ana_origin` VALUES (1, 'bqb', 'bqb', 'www.biqubao.com', 1, 1, 0, 'yyyy-MM-dd', '说明', 'UTF-8', 'html', 1, 0, 'UTF-8', NULL, 1, '2020-03-08 05:59:43', '2020-03-08 05:59:43');

-- ----------------------------
-- Records of ana_origin_detail
-- ----------------------------
INSERT INTO `ana_origin_detail` VALUES (1, 1, 1, 0, 0, NULL, 1, '2020-03-08 05:59:43', '2020-03-08 05:59:43');
INSERT INTO `ana_origin_detail` VALUES (2, 1, 2, 0, 0, NULL, 1, '2020-03-08 05:59:45', '2020-03-08 05:59:45');
INSERT INTO `ana_origin_detail` VALUES (3, 1, 3, 0, 0, NULL, 1, '2020-03-08 05:59:45', '2020-03-08 05:59:45');

-- ----------------------------
-- Records of ana_origin_detail_field
-- ----------------------------
INSERT INTO `ana_origin_detail_field` VALUES (1, 1, 0, 1, 'nextPageUrl', '//div[@class=\"search-result-page-main\"]/a[@title=\"下一页\"]/@href', '', '2020-03-08 05:59:43', '2020-03-08 05:59:43');
INSERT INTO `ana_origin_detail_field` VALUES (2, 1, 0, 0, 'books', '//div[@class=\"result-list\"]/div[@class=\"result-item result-game-item\"]', '', '2020-03-08 05:59:43', '2020-03-08 05:59:43');
INSERT INTO `ana_origin_detail_field` VALUES (3, 1, 2, 1, 'name', './div[@class=\"result-game-item-detail\"]/h3/a/span/text()', '', '2020-03-08 05:59:44', '2020-03-08 05:59:44');
INSERT INTO `ana_origin_detail_field` VALUES (4, 1, 2, 1, 'author', './div[@class=\"result-game-item-detail\"]/div/p[1]/span[2]/text()', '', '2020-03-08 05:59:44', '2020-03-08 05:59:44');
INSERT INTO `ana_origin_detail_field` VALUES (5, 1, 2, 1, 'summary', './div[@class=\"result-game-item-detail\"]/p/text()', '', '2020-03-08 05:59:44', '2020-03-08 05:59:44');
INSERT INTO `ana_origin_detail_field` VALUES (6, 1, 2, 1, 'titlePage', './div[@class=\"result-game-item-pic\"]/a/img/@src', '', '2020-03-08 05:59:44', '2020-03-08 05:59:44');
INSERT INTO `ana_origin_detail_field` VALUES (7, 1, 2, 1, 'type', './div[@class=\"result-game-item-detail\"]/div/p[2]/span[2]/text()', '', '2020-03-08 05:59:44', '2020-03-08 05:59:44');
INSERT INTO `ana_origin_detail_field` VALUES (8, 1, 2, 1, 'updated', './div[@class=\"result-game-item-detail\"]/div/p[3]/span[2]/text()', '', '2020-03-08 05:59:44', '2020-03-08 05:59:44');
INSERT INTO `ana_origin_detail_field` VALUES (9, 1, 2, 1, 'status', '', '', '2020-03-08 05:59:44', '2020-03-08 05:59:44');
INSERT INTO `ana_origin_detail_field` VALUES (10, 1, 2, 1, 'words', '', '', '2020-03-08 05:59:44', '2020-03-08 05:59:44');
INSERT INTO `ana_origin_detail_field` VALUES (11, 1, 2, 1, 'catalogUrl', './div[@class=\"result-game-item-detail\"]/h3/a/@href', '', '2020-03-08 05:59:44', '2020-03-08 05:59:44');
INSERT INTO `ana_origin_detail_field` VALUES (12, 1, 2, 1, 'lastCapterTitle', './div[@class=\"result-game-item-detail\"]/div/p[4]/a/text()', '', '2020-03-08 05:59:44', '2020-03-08 05:59:44');
INSERT INTO `ana_origin_detail_field` VALUES (13, 1, 2, 1, 'lastCapterUrl', './div[@class=\"result-game-item-detail\"]/div/p[4]/a/@href', '', '2020-03-08 05:59:45', '2020-03-08 05:59:45');
INSERT INTO `ana_origin_detail_field` VALUES (14, 2, 0, 1, 'nextPageUrl', '', '', '2020-03-08 05:59:45', '2020-03-08 05:59:45');
INSERT INTO `ana_origin_detail_field` VALUES (15, 2, 0, 0, 'chapters', '//div[@id=\"list\"]/dl/dd]', '', '2020-03-08 05:59:45', '2020-03-08 05:59:45');
INSERT INTO `ana_origin_detail_field` VALUES (16, 2, 15, 1, 'name', './a/text()', '', '2020-03-08 05:59:45', '2020-03-08 05:59:45');
INSERT INTO `ana_origin_detail_field` VALUES (17, 2, 15, 1, 'url', 'concat(\'https://www.biqubao.com\',./a/@href)', '', '2020-03-08 05:59:45', '2020-03-09 02:06:17');
INSERT INTO `ana_origin_detail_field` VALUES (18, 3, 0, 1, 'nextPageUrl', '', '', '2020-03-08 05:59:46', '2020-03-08 05:59:46');
INSERT INTO `ana_origin_detail_field` VALUES (19, 3, 0, 1, 'name', '//div[@class=\"bookname\"]/h1[1]/text()', '', '2020-03-08 05:59:46', '2020-03-08 05:59:46');
INSERT INTO `ana_origin_detail_field` VALUES (20, 3, 0, 1, 'content', '//div[@id=\"content\"]/text()', '', '2020-03-08 05:59:46', '2020-03-08 05:59:46');
INSERT INTO `ana_origin_detail_field` VALUES (21, 3, 0, 1, 'catalogUrl', '//div[@class=\"bookname\"]/div[@class=\"bottem1\"]/a[2]/@href', '', '2020-03-08 05:59:46', '2020-03-08 05:59:46');
INSERT INTO `ana_origin_detail_field` VALUES (22, 3, 0, 1, 'nextChapterUrl', '//div[@class=\"bookname\"]/div[@class=\"bottem1\"]/a[3]/@href', '', '2020-03-08 05:59:46', '2020-03-08 05:59:46');
INSERT INTO `ana_origin_detail_field` VALUES (23, 3, 0, 1, 'preChapterUrl', '//div[@class=\"bookname\"]/div[@class=\"bottem1\"]/a[1]/@href', '', '2020-03-08 05:59:46', '2020-03-08 05:59:46');

-- ----------------------------
-- Records of ana_origin_detail_request
-- ----------------------------
INSERT INTO `ana_origin_detail_request` VALUES (1, 1, 1, 'GET', 'https://www.biqubao.com/search.php', '{\"keyword\":\"${condition}\"}', 'UTF-8', 'UTF-8', 'UTF-8', 1, '2020-03-08 05:59:43', '2020-03-08 05:59:43');
INSERT INTO `ana_origin_detail_request` VALUES (2, 1, 2, 'GET', '', NULL, 'UTF-8', 'UTF-8', 'UTF-8', 1, '2020-03-08 05:59:43', '2020-03-08 05:59:43');
INSERT INTO `ana_origin_detail_request` VALUES (3, 2, 1, 'GET', '', NULL, 'GBK', 'GBK', 'GBK', 1, '2020-03-08 05:59:45', '2020-03-08 05:59:45');
INSERT INTO `ana_origin_detail_request` VALUES (4, 2, 2, 'GET', '', NULL, 'GBK', 'GBK', 'GBK', 1, '2020-03-08 05:59:45', '2020-03-08 05:59:45');
INSERT INTO `ana_origin_detail_request` VALUES (5, 3, 1, 'GET', '', NULL, 'GBK', 'GBK', 'GBK', 1, '2020-03-08 05:59:46', '2020-03-08 05:59:46');
INSERT INTO `ana_origin_detail_request` VALUES (6, 3, 2, 'GET', '', NULL, 'GBK', 'GBK', 'GBK', 1, '2020-03-08 05:59:46', '2020-03-08 05:59:46');
