CREATE TABLE `ana_book`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ana_origin_code` varchar(50) NOT NULL COMMENT '书源类型',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `author` varchar(20) NOT NULL COMMENT '作者',
  `title_page` varchar(255) NOT NULL DEFAULT 'https://www.baidu.com' COMMENT '封面',
  `words` int(11) NOT NULL DEFAULT 0 COMMENT '字数(个)',
  `summary` varchar(255) NOT NULL DEFAULT '作者很懒，什么都没有留下' COMMENT '摘要（简介）',
  `status` varchar(10) NOT NULL DEFAULT 'PREPARE' COMMENT '状态：PREPARE=带获取，LOADING=连载中，FINISH=已完本',
  `catalog_url` varchar(255) NOT NULL COMMENT '目录链接',
  `created` datetime(0) NOT NULL COMMENT '创建时间',
  `updated` timestamp(0) NOT NULL DEFAULT current_timestamp COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT = '书籍表';

-- ----------------------------
-- Table structure for ana_book_chapter
-- ----------------------------
CREATE TABLE `ana_book_chapter`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ana_book_id` int(11) NOT NULL COMMENT '所属书籍iid',
  `number` int(11) NOT NULL COMMENT '第几章',
  `name` varchar(50) NOT NULL COMMENT '章节名字',
  `words` int(11) NOT NULL DEFAULT 0 COMMENT '章节字数',
  `url` varchar(500) NOT NULL COMMENT '请求链接',
  `created` datetime(0) NOT NULL COMMENT '创建时间',
  `updated` timestamp(0) NOT NULL DEFAULT current_timestamp COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT = '章节表';

-- ----------------------------
-- Table structure for ana_user
-- ----------------------------
CREATE TABLE `ana_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `nickname` varchar(50) NOT NULL COMMENT '昵称',
  `password` varchar(128) NOT NULL COMMENT '密码',
  `head_img` varchar(255) NOT NULL COMMENT '头像（默认头像）',
  `phone_number` varchar(25) NULL DEFAULT NULL COMMENT '手机号',
  `created` datetime(0) NOT NULL,
  `updated` timestamp(0) NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE
) COMMENT = '用户表';

-- ----------------------------
-- Table structure for ana_user_book
-- ----------------------------
CREATE TABLE `ana_user_book`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ana_user_id` int(11) NOT NULL COMMENT '用户id',
  `ana_book_id` int(11) NOT NULL COMMENT '书籍id',
  `ana_book_chapter_id` int(11) NOT NULL DEFAULT 0 COMMENT '当前阅读章节id',
  `created` datetime(0) NOT NULL,
  `updated` timestamp(0) NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE
) COMMENT = '用户阅读书籍记录表';

-- ----------------------------
-- Table structure for ana_user_book
-- ----------------------------
CREATE TABLE `ana_user_bookshelf`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ana_user_id` int(11) NOT NULL COMMENT '用户id',
  `ana_book_id` int(11) NOT NULL COMMENT '书籍id',
  `ana_book_chapter_id` int(11) NOT NULL DEFAULT 0 COMMENT '当前阅读章节id',
  `created` datetime(0) NOT NULL,
  `updated` timestamp(0) NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE
) COMMENT = '用户书架表';

-- ----------------------------
-- Table structure for ana_user_customize
-- ----------------------------
CREATE TABLE `ana_user_customize`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `font` varchar(20) NOT NULL COMMENT '字体',
  `font_color` varchar(20) NOT NULL COMMENT '字体颜色',
  `font_size` int(11) NOT NULL COMMENT '字体大小',
  `background` varchar(20) NOT NULL COMMENT '背景颜色',
  `line_height` int(11) NOT NULL COMMENT '行高',
  `created` datetime(0) NOT NULL COMMENT '创建时间',
  `updated` timestamp(0) NOT NULL DEFAULT current_timestamp COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT = '用户自定义配置表';

-- 创建表格
CREATE TABLE `ana_origin` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`code` varchar(50) NOT NULL COMMENT '编码',
`name` varchar(50) NOT NULL COMMENT '名称',
`domain` varchar(255) NOT NULL COMMENT '域名',
`open_https` int(2) NOT NULL DEFAULT 0 COMMENT '是否使用https: 0=false,1=true',
`weights` int(10) NOT NULL DEFAULT 0 COMMENT '权重：越大权重越高',
`retry` int(2) NOT NULL DEFAULT 0 COMMENT '重试次数',
`date_pattern` varchar(50) NOT NULL DEFAULT 'yyyy-MM-dd HH:mm:ss' COMMENT '时间表达式',
`description` varchar(255) NULL COMMENT '数据源描述',
`charset` varchar(25) NOT NULL DEFAULT 'UTF-8' COMMENT '字符编码：GBK、UTF-8',
`resp_type` varchar(25) NOT NULL DEFAULT 'html' COMMENT '请求结果类型：json/html',
`dyn` int(2) NOT NULL DEFAULT 1 COMMENT '开启动态解析：0=false，1=true',
`open_dookies` int(2) NOT NULL DEFAULT 0 COMMENT '开启 cookies：0=false，1=true',
`url_encoder` varchar(25) NULL COMMENT 'url 参数编码',
`customize_header` varchar(255) NULL COMMENT '自定义请求头',
`status` int(2) NOT NULL DEFAULT 0 COMMENT '状态：0=不可用，1=可用，2=已删除',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`id`)
) COMMENT = '书源表';

CREATE TABLE `ana_origin_detail` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`ana_origin_id` int(10) NOT NULL COMMENT '数据源id',
`type` int(2) NOT NULL COMMENT '类型：1=书籍，2=目录，3=章节',
`weights` int(10) NOT NULL DEFAULT 0 COMMENT '权重：数值越大权重越高',
`retry` int(2) NOT NULL COMMENT '重试次数：0=不重试',
`date_pattern` varchar(50) NULL COMMENT '时间表达式',
`status` int(2) NOT NULL DEFAULT 0 COMMENT '状态：0=不可用，1=可用，2=已删除',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) COMMENT = '书源详情表';

CREATE TABLE `ana_origin_detail_request` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`ana_origin_detail_id` int(10) NOT NULL,
`type` int(2) NOT NULL DEFAULT 1 COMMENT '类型：1=常规请求，2=下一页，3=上一页',
`method` varchar(10) NOT NULL DEFAULT 'GET' COMMENT '请求方式',
`url` varchar(255) NULL COMMENT '请求链接',
`param` varchar(255) NULL COMMENT '请求参数',
`url_encoder` varchar(20) NULL COMMENT 'url 字符编码',
`in_charset` varchar(20) NULL COMMENT '输入字符编码',
`out_charset` varchar(20) NULL COMMENT '输出字符编码',
`status` int(2) NOT NULL COMMENT '状态：0=不可用，1=可用，2=已删除',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) COMMENT = '书源请求配置表';

CREATE TABLE `ana_origin_field` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`field` varchar(50) NOT NULL COMMENT '字段',
`description` varchar(255) NOT NULL COMMENT '字段描述',
`parent_id` int(10) NOT NULL DEFAULT 0 COMMENT '父节点id',
`is_leaf` int(2) NOT NULL DEFAULT 1 COMMENT '是否为叶子节点：0=否，1=是',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) COMMENT = '书源字段字典表';

CREATE TABLE `ana_origin_detail_field` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`ana_origin_detail_id` int(10) NOT NULL,
`parent_id` int(10) NOT NULL DEFAULT 0 COMMENT '父类id，没有时为0，默认为0',
`is_leaf` int(2) NOT NULL DEFAULT 1 COMMENT '是否时叶子节点：0=否，1=是。默认1',
`field` varchar(50) NOT NULL COMMENT '字段名',
`x_path` varchar(255) NOT NULL COMMENT '解析表达式：json时使用jsonpath，xml时使用 xpath',
`class_name` varchar(255) NULL COMMENT '类名，预留字段',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) COMMENT = '书源字段解析表';

ALTER TABLE `ana_origin_field`
ADD COLUMN `type` int(2) NOT NULL COMMENT '类型：1=书籍，2=目录，3=章节' AFTER `field`;

ALTER TABLE `ana_book`
ADD UNIQUE INDEX `ana_book_unique_idx_1`(`ana_origin_code`, `name`, `author`) USING BTREE COMMENT '来源，书名，作者唯一索引';

ALTER TABLE `ana_book`
ADD COLUMN `code` varchar(255) NULL COMMENT '唯一编码' AFTER `ana_origin_code`;

ALTER TABLE `ana_book_chapter`
CHANGE COLUMN `ana_book_id` `ana_book_code` varchar(255) NOT NULL COMMENT '所属书籍唯一编码' AFTER `id`,
ADD COLUMN `code` varchar(255) NOT NULL COMMENT '章节唯一编码' AFTER `ana_book_code`;

ALTER TABLE `ana_user_book`
CHANGE COLUMN `ana_book_id` `ana_book_code` varchar(255) NOT NULL COMMENT '书籍唯一编码' AFTER `ana_user_id`,
CHANGE COLUMN `ana_book_chapter_id` `ana_book_chapter_code` varchar(255) NOT NULL DEFAULT 0 COMMENT '当前阅读章节唯一编码' AFTER `ana_book_code`;

ALTER TABLE `ana_user_bookshelf`
CHANGE COLUMN `ana_book_id` `ana_book_code` varchar(255) NOT NULL COMMENT '书籍唯一编码' AFTER `ana_user_id`,
CHANGE COLUMN `ana_book_chapter_id` `ana_book_chapter_code` varchar(255) NOT NULL DEFAULT 0 COMMENT '当前阅读章节唯一编码' AFTER `ana_book_code`;

ALTER TABLE `ana_book_chapter`
ADD UNIQUE INDEX `ana_book_chapter_unique_idx_1`(`ana_book_code`, `name`) USING BTREE COMMENT '书籍章节唯一索引';

ALTER TABLE `ana_book`
ADD INDEX `ana_book_idx_1`(`code`) USING BTREE;

ALTER TABLE `ana_book_chapter`
ADD INDEX `ana_book_chapter_idx_1`(`code`) USING BTREE;

ALTER TABLE `ana_user_bookshelf`
ADD UNIQUE INDEX `ana_user_bookshelf_unique_idx_1`(`ana_user_id`, `ana_book_code`) USING BTREE COMMENT '用户书籍唯一索引';

ALTER TABLE `ana_user`
ADD COLUMN `status` int(2) NOT NULL COMMENT '状态：0=不可用，1=可用，2=已删除' AFTER `phone_number`;

ALTER TABLE `ana_user_book`
ADD COLUMN `status` int(2) NOT NULL COMMENT '状态：0=不可用，1=可用，2=已删除' AFTER `ana_book_chapter_code`;

ALTER TABLE `ana_user_bookshelf`
ADD COLUMN `status` int(2) NOT NULL COMMENT '状态：0=不可用，1=可用，2=已删除' AFTER `ana_book_chapter_code`;

ALTER TABLE `ana_book`
MODIFY COLUMN `created` datetime(0) NOT NULL DEFAULT current_timestamp COMMENT '创建时间' AFTER `catalog_url`,
MODIFY COLUMN `updated` timestamp(0) NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间' AFTER `created`;

ALTER TABLE `ana_book_chapter`
MODIFY COLUMN `created` datetime(0) NOT NULL DEFAULT current_timestamp COMMENT '创建时间' AFTER `url`,
MODIFY COLUMN `updated` timestamp(0) NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间' AFTER `created`;

ALTER TABLE `ana_user`
MODIFY COLUMN `created` datetime(0) NOT NULL DEFAULT current_timestamp AFTER `status`,
MODIFY COLUMN `updated` timestamp(0) NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP AFTER `created`;

ALTER TABLE `ana_user_book`
MODIFY COLUMN `created` datetime(0) NOT NULL DEFAULT current_timestamp AFTER `status`,
MODIFY COLUMN `updated` timestamp(0) NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP AFTER `created`;

ALTER TABLE `ana_user_bookshelf`
MODIFY COLUMN `created` datetime(0) NOT NULL DEFAULT current_timestamp AFTER `status`,
MODIFY COLUMN `updated` timestamp(0) NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP AFTER `created`;

ALTER TABLE `ana_user_customize`
MODIFY COLUMN `created` datetime(0) NOT NULL DEFAULT current_timestamp COMMENT '创建时间' AFTER `line_height`,
MODIFY COLUMN `updated` timestamp(0) NOT NULL DEFAULT current_timestamp ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间' AFTER `created`;

ALTER TABLE `ana_user_book`
ADD UNIQUE INDEX `ana_user_book_unique_idx_1`(`ana_user_id`, `ana_book_code`) USING BTREE;

ALTER TABLE `ana_book_chapter`
ADD COLUMN `content` text NULL COMMENT '内容' AFTER `url`;

ALTER TABLE `ana_user`
ADD UNIQUE INDEX `ana_user_unique_idx_1`(`username`) USING BTREE COMMENT '用户名唯一索引',
ADD UNIQUE INDEX `ana_user_unique_idx_2`(`phone_number`) USING BTREE COMMENT '手机号唯一索引';

ALTER TABLE `ana_user`
MODIFY COLUMN `phone_number` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号' AFTER `head_img`;
